<?php
// created: 2014-03-28 15:45:52
$dictionary['Lead']['fields']['gender']=array (
	'name' => 'gender',
	'vname' => 'LBL_GENDER',
	'type' => 'enum',
	'massupdate' => 0,
	'default' => ' ',
	'comments' => '',
	'help' => '',
	'importable' => 'true',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'reportable' => true,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'calculated' => false,
	'len' => 20,
	'size' => '20',
	'options' => 'gender_lead_list',
	'studio' => 'visible',
	'dbType' => 'enum',
	'required'=>true,
);
$dictionary['Lead']['fields']['nationality']=array (
	'name' => 'nationality',
	'vname' => 'LBL_NATIONALITY',
	'type' => 'varchar',
	'len' => '100',
	'comment' => '',
	'merge_filter' => 'disabled',
);
$dictionary['Lead']['fields']['occupation']=array (
	'name' => 'occupation',
	'vname' => 'LBL_OCCUPATION',
	'type' => 'varchar',
	'len' => '255',
	'comment' => ''
);
$dictionary['Lead']['fields']['potential']=array (
	'name' => 'potential',
	'vname' => 'LBL_POTENTIAL',
	'type' => 'enum',
	'comments' => '',
	'help' => '',
	'default' => 'Interested',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => 20,
	'size' => '20',
	'options' => 'level_lead_list',
	'studio' => 'visible',
);
$dictionary['Lead']['fields']['guardian_name']=array (
	'name' => 'guardian_name',
	'vname' => 'LBL_GUARDIAN_NAME',
	'type' => 'varchar',
	'len' => '100',
	'comment' => '',
	'merge_filter' => 'disabled',
);

$dictionary['Lead']['fields']['guardian_email']=array (
	'name' => 'guardian_email',
	'vname' => 'LBL_GUARDIAN_EMAIL',
	'type' => 'varchar',
	'len' => '100',
	'comment' => '',
	'merge_filter' => 'disabled',
);

$dictionary['Lead']['fields']['guardian_phone']=array (
	'name' => 'guardian_phone',
	'vname' => 'LBL_GUARDIAN_PHONE',
	'type' => 'phone',
	'dbType' => 'varchar',
	'len' => '50',
);

$dictionary["Lead"]["fields"]["school_name"] = array (
	'name' => 'school_name',
	'vname' => 'LBL_SCHOOL_NAME',
	'type' => 'varchar',
	'options' => 'schools_list',
	'len' => '200',
);

$dictionary['Lead']['fields']['nick_name']=array (
	'name' => 'nick_name',
	'vname' => 'LBL_NICK_NAME',
	'type' => 'varchar',
	'len' => '100',
	'comment' => ''
);
$dictionary['Lead']['fields']['other_mobile']=array (
	'name' => 'other_mobile',
	'vname' => 'LBL_OTHER_MOBILE',
	'type' => 'phone',
	'dbType' => 'varchar',
	'len' => '50',
);
// Relationship Lead ( 1 - n ) Delivery Revenue
$dictionary['Lead']['relationships']['lead_revenue'] = array(
	'lhs_module'        => 'Leads',
	'lhs_table'            => 'leads',
	'lhs_key'            => 'id',
	'rhs_module'        => 'C_DeliveryRevenue',
	'rhs_table'            => 'c_deliveryrevenue',
	'rhs_key'            => 'lead_id',
	'relationship_type'    => 'one-to-many',
);

$dictionary['Lead']['fields']['lead_revenue'] = array(
	'name' => 'lead_revenue',
	'type' => 'link',
	'relationship' => 'lead_revenue',
	'module' => 'C_DeliveryRevenue',
	'bean_name' => 'C_DeliveryRevenue',
	'source' => 'non-db',
	'vname' => 'LBL_DELIVERY_REVENUE',
);
//END: Relationship Lead ( 1 - n ) Delivery Revenue

// Relationship Lead ( 1 - n ) Carry Forward
$dictionary['Lead']['relationships']['lead_forward'] = array(
	'lhs_module'        => 'Leads',
	'lhs_table'            => 'leads',
	'lhs_key'            => 'id',
	'rhs_module'        => 'C_Carryforward',
	'rhs_table'            => 'c_carryforward',
	'rhs_key'            => 'lead_id',
	'relationship_type'    => 'one-to-many',
);

$dictionary['Lead']['fields']['lead_forward'] = array(
	'name' => 'lead_forward',
	'type' => 'link',
	'relationship' => 'lead_forward',
	'module' => 'C_Carryforward',
	'bean_name' => 'C_Carryforward',
	'source' => 'non-db',
	'vname' => 'LBL_CARRYFORWARD',
);
//END: Relationship Student ( 1 - n ) Carry Forward

$dictionary['Lead']['fields']['last_name']['required']=true;
$dictionary['Lead']['fields']['phone_mobile']['required']=true;

$dictionary['Lead']['fields']['preferred_kind_of_course'] = array (
	'name' => 'preferred_kind_of_course',
	'vname' => 'LBL_PREFERRED_KIND_OF_COURSE',
	'type' => 'enum',
	'default' => '',
	'comments' => 'comment',
	'help' => 'help',
	'importable' => 'true',
	'duplicate_merge' => 'enabled',
	'duplicate_merge_dom_value' => '1',
	'audited' => true,
	'reportable' => true,
	'len' => 100,
	'size' => '20',
	'options' => 'full_kind_of_course_list',
	'studio' => 'visible',
	'dependency' => false,
	'massupdate' => false,
);
$dictionary['Lead']['fields']['custom_button'] = array (
	'name' => 'custom_button',
	'vname' => 'LBL_CUSTOM_BUTTON',
	'type' => 'varchar',
	'studio' => 'visible',
	'source' => 'non-db',
);

//add team type
$dictionary['Lead']['fields']['team_type'] = array(
	'name' => 'team_type',
	'vname' => 'LBL_TEAM_TYPE',
	'type' => 'enum',
	'importable' => 'true',
	'reportable' => true,
	'len' => 100,
	'size' => '20',
	'options' => 'type_team_list',
	'studio' => 'visible',
    'massupdate' => 0,
);
// END: add team type

//Custom Relationship JUNIOR. Student - SMS  By Lap Nguyen
$dictionary['Lead']['fields']['leads_sms'] = array (
    'name' => 'leads_sms',
    'type' => 'link',
    'relationship' => 'lead_smses',
    'module' => 'C_SMS',
    'bean_name' => 'C_SMS',
    'source' => 'non-db',
    'vname' => 'LBL_LEAD_SMS',
);
$dictionary['Lead']['relationships']['lead_smses'] = array (
    'lhs_module'        => 'Leads',
    'lhs_table'            => 'leads',
    'lhs_key'            => 'id',
    'rhs_module'        => 'C_SMS',
    'rhs_table'            => 'c_sms',
    'rhs_key'            => 'parent_id',
    'relationship_type'    => 'one-to-many',
);

$dictionary['Lead']['fields']['contact_rela'] = array (
	'name' => 'contact_rela',
	'vname' => 'LBL_CONTACT_RELA',
	'type' => 'enum',
	'duplicate_merge' => 'disabled',
	'duplicate_merge_dom_value' => '0',
	'audited' => false,
	'unified_search' => false,
	'merge_filter' => 'disabled',
	'len' => 20,
	'size' => '20',
	'options' => 'rela_contacts_list',
	'studio' => 'visible',
	'massupdate' => 0,
);
$dictionary["Lead"]["fields"]["relationship"] = array (
	'name'      => 'relationship',
	'vname'     => 'LBL_RELATIONSHIP',
	'type'      => 'text',
	'source' => 'non-db',
	'studio'    => 'visible',
);
$dictionary["Lead"]["fields"]["describe_relationship"] = array (
	'name'      => 'describe_relationship',
	'vname'     => 'LBL_DESCRIBE_RELATIONSHIP',
	'type'      => 'text',
	'help' => 'help',
	'importable' => 'true',
	'duplicate_merge' => 'enabled',
	'duplicate_merge_dom_value' => '1',
	'reportable' => true,
	'size' => '20',
	'studio' => 'visible',
	'rows' => '4',
	'cols' => '40',
);
//Custom Relationship JUNIOR. Student - StudentSituation  By Nhi Vo
$dictionary['Lead']['fields']['ju_studentsituations'] = array (
	'name' => 'ju_studentsituations',
	'type' => 'link',
	'relationship' => 'lead_studentsituations',
	'module' => 'J_StudentSituations',
	'bean_name' => 'J_StudentSituations',
	'source' => 'non-db',
	'vname' => 'LBL_LEAD_SITUATION',
);
$dictionary['Lead']['relationships']['lead_studentsituations'] = array (
	'lhs_module'        => 'Leads',
	'lhs_table'            => 'leads',
	'lhs_key'            => 'id',
	'rhs_module'        => 'J_StudentSituations',
	'rhs_table'            => 'j_studentsituations',
	'rhs_key'            => 'lead_id',
	'relationship_type'    => 'one-to-many',
);

//Add Relationship Lead - Payment (Thu tiền Placement Test)
$dictionary['Lead']['relationships']['lead_payments'] = array(
    'lhs_module' => 'Leads',
    'lhs_table' => 'leads',
    'lhs_key' => 'id',
    'rhs_module' => 'J_Payment',
    'rhs_table' => 'j_payment',
    'rhs_key' => 'lead_id',
    'relationship_type' => 'one-to-many'
);
$dictionary['Lead']['fields']['payment_link'] = array(
    'name' => 'payment_link',
    'type' => 'link',
    'relationship' => 'lead_payments',
    'module' => 'J_Payment',
    'bean_name' => 'J_Payment',
    'source' => 'non-db',
    'vname' => 'LBL_PAYMENT_NAME',
);