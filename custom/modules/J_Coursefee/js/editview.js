$(document).ready(function(){
    toggleCourseFee();
    $('#type').on('change',function(){
        $('#type_of_course_fee').val('');
        toggleCourseFee();
    });

});
function toggleCourseFee(){
    var type = $('#type').val();
    $("#type_of_course_fee option:not(:first)").each(function(){
        if(type == 'Days'){
            if($(this).val().indexOf('days') != -1)
                $(this).show();
            else $(this).hide();
        }else if(type == 'Sessions'){
            if($(this).val().indexOf('sessions') != -1)
                $(this).show();
            else $(this).hide();
        }else if(type == 'Hours'){
            if($(this).val().indexOf('hours') != -1)
                $(this).show();
            else $(this).hide();
        }
    });

}