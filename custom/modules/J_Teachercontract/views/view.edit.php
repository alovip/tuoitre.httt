<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class J_TeachercontractViewEdit extends ViewEdit
{
    public function __construct()
    {
        parent::ViewEdit();
        $this->useForSubpanel = true;
        $this->useModuleQuickCreateTemplate = true;
    }
    public function display()
    {
        if($_POST['isDuplicate'] == 'true'){
            $this->bean->name         = translate('LBL_AUTO_GENERATE','Accounts');
        }else{
            if(empty($this->bean->id))
                $this->bean->name     = translate('LBL_AUTO_GENERATE','Accounts');
        }
        parent::display();
    }
}