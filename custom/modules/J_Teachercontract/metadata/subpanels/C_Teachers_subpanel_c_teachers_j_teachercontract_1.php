<?php
// created: 2017-09-26 00:18:15
$subpanel_layout['list_fields'] = array (
  'name' =>
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '15%',
    'default' => true,
  ),
  'contract_type' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CONTRACT_TYPE',
    'width' => '10%',
  ),
  'contract_date' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_CONTRACT_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'contract_until' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_CONTRACT_UNTIL',
    'width' => '10%',
    'default' => true,
  ),
  'status' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'time_up' =>
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_TIME_UP',
    'width' => '5%',
  ),  
  'price' =>
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_PRICE',
    'width' => '10%',
  ),
  'date_modified' =>
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'team_name' =>
  array (
    'type' => 'relate',
    'link' => true,
    'studio' =>
    array (
      'portallistview' => false,
      'portaldetailview' => false,
      'portaleditview' => false,
    ),
    'vname' => 'LBL_TEAMS',
    'id' => 'TEAM_ID',
    'width' => '12%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'Teams',
    'target_record_key' => 'team_id',
  ),
  'edit_button' =>
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'J_Teachercontract',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' =>
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'J_Teachercontract',
    'width' => '5%',
    'default' => true,
  ),
);