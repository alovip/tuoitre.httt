<?php
$popupMeta = array (
    'moduleMain' => 'C_Contacts',
    'varName' => 'C_Contacts',
    'orderBy' => 'c_contacts.name',
    'whereClauses' => array (
  'name' => 'c_contacts.name',
  'mobile_phone' => 'c_contacts.mobile_phone',
  'accounts_c_contacts_1_name' => 'c_contacts.accounts_c_contacts_1_name',
  'key_point_text' => 'c_contacts.key_point_text',
  'email1' => 'c_contacts.email1',
  'favorites_only' => 'c_contacts.favorites_only',
),
    'searchInputs' => array (
  1 => 'name',
  5 => 'mobile_phone',
  7 => 'accounts_c_contacts_1_name',
  8 => 'key_point_text',
  9 => 'email1',
  10 => 'favorites_only',
),
    'create' => array (
  'formBase' => 'FormBase.php',
  'formBaseClass' => 'FormBase',
  'createButton' => 'Create',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'mobile_phone' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10%',
    'name' => 'mobile_phone',
  ),
  'accounts_c_contacts_1_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_C_CONTACTS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_C_CONTACTS_1ACCOUNTS_IDA',
    'width' => '10%',
    'name' => 'accounts_c_contacts_1_name',
  ),
  'key_point_text' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_KEY_POINT_TEXT',
    'sortable' => false,
    'width' => '10%',
    'name' => 'key_point_text',
  ),
  'email1' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_EMAIL_ADDRESS',
    'width' => '10%',
    'name' => 'email1',
  ),
  'favorites_only' => 
  array (
    'name' => 'favorites_only',
    'label' => 'LBL_FAVORITES_FILTER',
    'type' => 'bool',
    'width' => '10%',
  ),
),
    'listviewdefs' => array (
  'NAME' => 
  array (
    'width' => '12%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'MOBILE_PHONE' => 
  array (
    'type' => 'phone',
    'label' => 'LBL_MOBILE_PHONE',
    'width' => '10%',
    'default' => true,
    'name' => 'mobile_phone',
  ),
  'EMAIL1' => 
  array (
    'type' => 'varchar',
    'studio' => 
    array (
      'editview' => true,
      'editField' => true,
      'searchview' => false,
      'popupsearch' => false,
    ),
    'label' => 'LBL_EMAIL_ADDRESS',
    'width' => '10%',
    'default' => true,
    'name' => 'email1',
  ),
  'ACCOUNTS_C_CONTACTS_1_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_ACCOUNTS_C_CONTACTS_1_FROM_ACCOUNTS_TITLE',
    'id' => 'ACCOUNTS_C_CONTACTS_1ACCOUNTS_IDA',
    'width' => '10%',
    'default' => true,
    'name' => 'accounts_c_contacts_1_name',
  ),
  'POSITION' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_POSITION',
    'width' => '10%',
    'default' => true,
    'name' => 'position',
  ),
  'KEY_POINT_TEXT' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_KEY_POINT_TEXT',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
    'name' => 'key_point_text',
  ),
  'TEAM_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_TEAM',
    'default' => true,
    'name' => 'team_name',
  ),
),
);
