<?php
$module_name = 'C_Contacts';
$viewdefs[$module_name] =
array (
  'QuickCreate' =>
  array (
    'templateMeta' =>
    array (
      'maxColumns' => '2',
      'widths' =>
      array (
        0 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' =>
      array (
        'DEFAULT' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' =>
    array (
      'default' =>
      array (
        0 =>
        array (
          0 => 'name',
          1 =>
          array (
            'name' => 'mobile_phone',
            'label' => 'LBL_MOBILE_PHONE',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'email1',
            'studio' =>
            array (
              'editview' => true,
              'editField' => true,
              'searchview' => false,
              'popupsearch' => false,
            ),
            'label' => 'LBL_EMAIL_ADDRESS',
          ),
          1 =>
          array (
            'name' => 'address',
            'studio' => 'visible',
            'label' => 'LBL_ADDRESS',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'position',
            'label' => 'LBL_POSITION',
          ),
          1 =>
          array (
            'name' => 'accounts_c_contacts_1_name',
            'label' => 'LBL_ACCOUNTS_C_CONTACTS_1_FROM_ACCOUNTS_TITLE',
          ),
        ),
        3 =>
        array (
          0 =>
          array (
            'name' => 'key_point_text',
            'studio' => 'visible',
            'label' => 'LBL_KEY_POINT_TEXT',
            'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 60,
                        ),
          ),
          1=> 'birthdate'
        ),
        4 =>
        array (
          0 => 'description',
        ),
      ),
      'lbl_editview_panel1' =>
      array (
        0 =>
        array (
          0 => 'assigned_user_name',
          1 =>
          array (
            'name' => 'team_name',
            'displayParams' =>
            array (
              'display' => true,
            ),
          ),
        ),
      ),
    ),
  ),
);
