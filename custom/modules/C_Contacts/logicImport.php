<?php
class logicImportC_Contacts{
    //Import Payment
    function importC_Contacts(&$bean, $event, $arguments){
        global $timedate;
        if($_POST['module'] == 'Import'){
            $rs     = $GLOBALS['db']->query("SELECT id, team_id, team_set_id FROM accounts WHERE account_id='{$bean->account_id}' AND deleted = 0");
            $acc    = $GLOBALS['db']->fetchByAssoc($rs);
            if(!empty($acc['id'])){
                $count = 0;
                $q1 = "SELECT DISTINCT
                IFNULL(c_contacts.id, '') primaryid,
                IFNULL(c_contacts.name, '') c_contacts_name
                FROM
                c_contacts
                INNER JOIN
                accounts_c_contacts_1_c l1_1 ON c_contacts.id = l1_1.accounts_c_contacts_1c_contacts_idb
                AND l1_1.deleted = 0
                INNER JOIN
                accounts l1 ON l1.id = l1_1.accounts_c_contacts_1accounts_ida
                AND l1.deleted = 0
                WHERE
                (((l1.id = '{$acc['id']}')))
                AND c_contacts.deleted = 0";
                $rs1 = $GLOBALS['db']->query($q1);
                while($row = $GLOBALS['db']->fetchByAssoc($rs1)){
                    if($row['c_contacts_name'] == $bean->name)
                        $count++;
                }
                if($count == 0){
                    if($bean->load_relationship('accounts_c_contacts_1'))
                        $bean->accounts_c_contacts_1->add($acc['id']);
                    $bean->team_id      = $acc['team_id'];
                    $bean->team_set_id  = $acc['team_set_id'];
                }else{
                    $bean->deleted = 1;
                }

            }else
                $bean->deleted = 1;

        }
    }
}

?>
