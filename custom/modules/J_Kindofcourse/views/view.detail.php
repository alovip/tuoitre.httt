<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/MVC/View/views/view.detail.php');

class J_KindofcourseViewDetail extends ViewDetail {
    function J_KindofcourseViewDetail(){
        parent::ViewDetail();
    }
    function display() {
        //get list levels to array
        $tmpContent = json_decode(html_entity_decode($this->bean->content),true);
        $levelConfigHtml = '<table style="width:40%"><thead><tr>';
        $levelConfigHtml .= '<th>'.translate('LBL_LEVEL','J_Kindofcourse').'</th>';
        $levelConfigHtml .= '<th>'.translate('LBL_IS_SET_HOUR','J_Kindofcourse').'</th>';
        $levelConfigHtml .= '<th>'.translate('LBL_HOURS','J_Kindofcourse').'</th>';
        $levelConfigHtml .= '</tr></thead><tbody>';

        foreach($tmpContent as $key => $value){
            $levelConfigHtml .= '<tr>';
            $levelConfigHtml .= '<td style="text-align:center;width:20%"> '.(($value['levels'] == '') ? '-none-' : $value['levels']).'</td>';
            $levelConfigHtml .= '<td style="text-align:center;width:20%"> '.(($value['is_set_hour'] == '1') ? '<input type="checkbox" name="is_set_hour" checked disabled>' : '<input type="checkbox" name="is_set_hour" disabled>').' </td>';
            $levelConfigHtml .= '<td style="text-align:center;width:15%"> '.$value['hours'].'</td>';
            $levelConfigHtml .= '</tr>';
        }
        $levelConfigHtml .= '</tbody></table>';

        $this->ss->assign('LEVEL_CONFIG',$levelConfigHtml);
        $team_type = getTeamType($this->bean->team_id);
        $this->ss->assign('team_type',$team_type);
        parent::display();
    }
}
?>