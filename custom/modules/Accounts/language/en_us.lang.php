<?php
// created: 2015-08-12 09:16:05
$mod_strings = array (
  'LBL_CONTACTS' => 'Students',
  'LBL_OPPORTUNITY' => 'Enrollment',
  'LBL_BANK_NAME' => 'Bank name',
  'LBL_BANK_NUMBER' => 'Bank number',
  'LBL_TAX_CODE' => 'Tax code',
  'LBL_MEMBERS' => 'Members',
  'LBL_ACCOUNT_ID' => 'Corporate ID',
  'LBL_ACCOUNT_STATUS' => 'Status',
  'LBL_CASES' => 'Feedbacks',
  'LBL_TYPE_OF_ACCOUNT' => 'Type',
  'LBL_MEETINGS' => 'Meetings',
  'LBL_PAYMENT_NAME' => 'Payment',
  'LBL_STREET' => 'Street',
  'LBL_STATE' => 'State',
  'LBL_TEAM_NAME' => 'Center',
  'LBL_TEAM' => 'Center',
  'LBL_ASSIGNED_TO' => 'Assigned To',



  'LBL_PANEL_ADVANCED' => 'More Information',
  'LBL_EDITVIEW_PANEL1' => 'History',
);