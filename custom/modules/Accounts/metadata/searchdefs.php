<?php
$searchdefs['Accounts'] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'account_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ACCOUNT_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'account_id',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'team_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'studio' => 
        array (
          'portallistview' => false,
          'portaldetailview' => false,
          'portaleditview' => false,
        ),
        'label' => 'LBL_TEAM',
        'id' => 'TEAM_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'team_name',
      ),
      'type_of_account' => 
      array (
        'type' => 'enum',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_ACCOUNT_TYPE',
        'width' => '10%',
        'name' => 'type_of_account',
      ),
    ),
    'advanced_search' => 
    array (
      'account_id' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_ACCOUNT_ID',
        'width' => '10%',
        'default' => true,
        'name' => 'account_id',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'phone' => 
      array (
        'name' => 'phone',
        'label' => 'LBL_ANY_PHONE',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'email' => 
      array (
        'name' => 'email',
        'label' => 'LBL_ANY_EMAIL',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'ceo_name' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_CEO_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'ceo_name',
      ),
      'address_street' => 
      array (
        'name' => 'address_street',
        'label' => 'LBL_ANY_ADDRESS',
        'type' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'full_name' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_FULL_NAME',
        'width' => '10%',
        'default' => true,
        'name' => 'full_name',
      ),
      'tax_code' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_TAX_CODE',
        'width' => '10%',
        'default' => true,
        'name' => 'tax_code',
      ),
      'assigned_user_id' => 
      array (
        'name' => 'assigned_user_id',
        'type' => 'enum',
        'label' => 'LBL_ASSIGNED_TO',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
        'default' => true,
        'width' => '10%',
      ),
      'team_name' => 
      array (
        'type' => 'relate',
        'link' => true,
        'studio' => 
        array (
          'portallistview' => false,
          'portaldetailview' => false,
          'portaleditview' => false,
        ),
        'label' => 'LBL_TEAMS',
        'width' => '10%',
        'default' => true,
        'id' => 'TEAM_ID',
        'name' => 'team_name',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
