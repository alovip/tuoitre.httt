<tr class="{$PRIORITY_LEVEL}">
    <td style="text-align: center;">
        <input type="radio" name="teacher_schedule_radio" value='{$TEACHER_ID}'>
    </td>
    <td>
        {$NAME}
        <input type="hidden" class="schedule_contract_id" value='{$CONTRACT_ID}'/>
    </td>
    <td>{$CONTRACT_TYPE}</td>
    <td>{$CONTRACT_NAME}</td>
    <td>
        {$CONTRACT_DATE}
        <input type="hidden" class="schedule_contract_date" value='{$CONTRACT_DATE}'/>
    </td>
    <td>
        {$EXPIRE_DAY}
        <input type="hidden" class="schedule_contract_until" value='{$EXPIRE_DAY}'/>
    </td>
    <td style="text-align: center;">{$TAUGHT_HOURS}</td>
</tr>