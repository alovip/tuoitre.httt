<?php
// created: 2017-09-04 16:41:43
$mod_strings = array (
  'LBL_STUDENT_NAME' => 'Học viên Name',
  'LNK_NEW_RECORD' => 'Tạo mới',
  'LNK_LIST' => 'Xem điểm danh',
  'LBL_MODULE_NAME' => 'Điểm danh',
  'LBL_NEW_FORM_TITLE' => 'Tạo mới',
  'LNK_IMPORT_C_ATTENDANCE' => 'Import từ file',
  'LBL_LIST_FORM_TITLE' => 'Xem điểm danh',
  'LBL_SEARCH_FORM_TITLE' => 'Tìm kiếm điểm danh',
  'LBL_MEETING_NAME' => 'Lịch Name',
);