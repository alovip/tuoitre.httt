<?php
$GLOBALS['log']->info('----->Scheduler fired job of type pruneDatabase()');
$backupDir    = sugar_cached('backups');
$backupFile    = 'backup-pruneDatabase-GMT0_'.gmdate('Y_m_d-H_i_s', strtotime('now')).'.php';

$db = DBManagerFactory::getInstance();
$tables = $db->getTablesArray();
$queryString = array();

if(!empty($tables)) {
    foreach($tables as $kTable => $table) {
        // find tables with deleted=1
        $columns = $db->get_columns($table);
        // no deleted - won't delete
        if(empty($columns['deleted'])) continue;

        $custom_columns = array();
        if(array_search($table.'_cstm', $tables)) {
            $custom_columns = $db->get_columns($table.'_cstm');
            if(empty($custom_columns['id_c'])) {
                $custom_columns = array();
            }
        }

        $qDel = "SELECT * FROM $table WHERE deleted = 1";
        $rDel = $db->query($qDel);

        // make a backup INSERT query if we are deleting.
        while($aDel = $db->fetchByAssoc($rDel, false)) {
            // build column names

            $queryString[] = $db->insertParams($table, $columns, $aDel, null, false);

            if(!empty($custom_columns) && !empty($aDel['id'])) {
                $qDelCstm = 'SELECT * FROM '.$table.'_cstm WHERE id_c = '.$db->quoted($aDel['id']);
                $rDelCstm = $db->query($qDelCstm);

                // make a backup INSERT query if we are deleting.
                while($aDelCstm = $db->fetchByAssoc($rDelCstm)) {
                    $queryString[] = $db->insertParams($table, $custom_columns, $aDelCstm, null, false);
                } // end aDel while()

                $db->query('DELETE FROM '.$table.'_cstm WHERE id_c = '.$db->quoted($aDel['id']));
            }
        } // end aDel while()
        // now do the actual delete
        $db->query('DELETE FROM '.$table.' WHERE deleted = 1');
    } // foreach() tables

    if(!file_exists($backupDir) || !file_exists($backupDir.'/'.$backupFile)) {
        // create directory if not existent
        mkdir_recursive($backupDir, false);
    }
    // write cache file

    write_array_to_file('pruneDatabase', $queryString, $backupDir.'/'.$backupFile);
    return true;
}
return false;