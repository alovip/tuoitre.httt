<?php
// created: 2015-12-28 10:05:55
global $current_user;
$subpanel_layout['list_fields'] = array (
    'name' =>
    array (
        'vname' => 'LBL_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'width' => '10%',
        'default' => true,
        'link' => true,
    ),
    'payment_type' =>
    array (
        'vname' => 'LBL_PAYMENT_TYPE',
        'width' => '7%',
        'default' => true,
    ),
    'class_string' =>
    array (
        'vname' => 'LBL_CLASSES_NAME',
        'width' => '10%',
        'default' => true,
        'sortable' => false,
    ),
    'payment_date' =>
    array (
        'vname' => 'LBL_PAYMENT_DATE',
        'width' => '7%',
        'default' => true,
    ),
    'total_amount' =>
    array (
        'vname' => 'LBL_TOTAL_AMOUNT',
        'width' => '7%',
        'sortable' => false,
        'default' => true,
    ),
    'total_sessions' =>
    array (
        'vname' => 'LBL_TOTAL_SESSIONS',
        'width' => '10%',
        'default' => true,
    ),

    'remain_amount' =>
    array (
        'vname' => 'LBL_REMAIN_AMOUNT',
        'width' => '7%',
        'sortable' => false,
        'default' => true,
        'align' => 'left',
    ),
    'remain_sessions' =>
    array (
        'vname' =>'LBL_REMAIN_SESSIONS',
        'width' => '10%',
        'default' => true,
    ),
    'assigned_user_name' =>
    array (
        'width' => '10%',
        'vname' => 'LBL_ASSIGNED_TO_NAME',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
    ),
    'team_name' =>
    array (
        'width' => '10%',
        'vname' => 'LBL_TEAM',
        'widget_class' => 'SubPanelDetailViewLink',
        'default' => true,
        'sortable' => false,
    ),
    'currency_id' =>
    array (
        'name' => 'currency_id',
        'usage' => 'query_only',
    ),
    'aims_id' =>
    array (
        'name' => 'aims_id',
        'usage' => 'query_only',
    ),
    'payment_amount' =>
    array (
        'name' => 'payment_amount',
        'usage' => 'query_only',
    ),
    'paid_amount' =>
    array (
        'name' => 'paid_amount',
        'usage' => 'query_only',
    ),
    'deposit_amount' =>
    array (
        'name' => 'deposit_amount',
        'usage' => 'query_only',
    ),
    'contract_id' =>
    array (
        'name' => 'contract_id',
        'usage' => 'query_only',
    ),
);
