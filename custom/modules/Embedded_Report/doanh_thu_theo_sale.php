<?php
$rowID = $GLOBALS['db']->fetchArray($this->query);

$this->query_list[0] = str_replace(",IFNULL(contracts.id,'') primaryid","",$this->query_list[0]);
$this->query_list[0] = str_replace(",IFNULL( contracts.currency_id,'') CONTRACTS_TOTAL_CONTRAE75D5E","",$this->query_list[0]);
$this->query_list[0] = str_replace(",contracts.total_contract_value CONTRACTS_TOTAL_CONTRA1E104D",",SUM(contracts.total_contract_value) total_contract_value",$this->query_list[0]);
$this->query_list[0] = str_replace("ORDER BY l2_full_user_name","GROUP BY l2_id ORDER BY l2_full_user_name",$this->query_list[0]);

$rows = $GLOBALS['db']->fetchArray($this->query_list[0]);
$html = '';
$html .= '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="reportlistView"><tbody>';
$html .= '<tr height="20">';
$html .= '<th align="center" class="" valign="middle" nowrap="">No.</th>';
$html .= '<th align="center" class="" valign="middle" nowrap="">Sale</th>';
$html .= '<th align="center" class="" valign="middle" nowrap="">User name</th>';
$html .= '<th align="center" class="" valign="middle" nowrap="">Doanh thu thực hiện</th>';
$html .= '<th align="center" class="" valign="middle" nowrap="">Doanh thu ký hợp đồng</th>';
$html .= '<th align="center" class="" valign="middle" nowrap="">Tỷ trọng</th></tr>';
$total_thuc_hien    = 0;
$total_hop_dong     = 0;
$ext_contract = "AND (l6.id IN('".implode("','",array_column($rowID,'primaryid'))."'))";
$q1="SELECT DISTINCT
IFNULL(l6.assigned_user_id, '') assigned_user_id,
AVG(IFNULL((l3.total_amount / l3.total_session), 0)) cost_per_hour,
SUM(IFNULL((l3.total_amount / l3.total_session * 1),0)) delivery_revenue
FROM
meetings
INNER JOIN
meetings_contacts l1_1 ON meetings.id = l1_1.meeting_id
AND l1_1.deleted = 0
INNER JOIN
contacts l1 ON l1.id = l1_1.contact_id
AND l1.deleted = 0
INNER JOIN
j_class l2 ON meetings.ju_class_id = l2.id
AND l2.deleted = 0
INNER JOIN
j_studentsituations l3 ON l1_1.situation_id = l3.id
AND l3.deleted = 0
AND l3.type IN ('Enrolled' , 'Moving In', 'Stopped')
INNER JOIN
j_payment l4 ON l3.payment_id = l4.id
AND l4.payment_type = 'Corporate'
AND l4.deleted = 0
INNER JOIN
contracts l6 ON l4.contract_id = l6.id AND l6.deleted = 0
INNER JOIN
teams l5 ON meetings.team_id = l5.id
AND l5.deleted = 0
AND l5.team_type = 'Junior'
WHERE
((meetings.deleted = 0
AND (meetings.session_status <> 'Cancelled')
$ext_contract))
GROUP BY assigned_user_id";
$rs1 = $GLOBALS['db']->query($q1);
$total_revenue  = 0;
$rowRes = array();
while($_row = $GLOBALS['db']->fetchbyAssoc($rs1)){
    $total_revenue += $_row['delivery_revenue'];
    $rowRes[$_row['assigned_user_id']] = $_row['delivery_revenue'];
}
foreach( $rows as $key => $row){
    $key++;
    $html .= "<tr>";
    $html .= "<td nowrap>".$key."</td>";
    $html .= "<td nowrap>".$row['l2_full_user_name']."</td>";
    $html .= "<td nowrap>".$row['l2_user_name']."</td>";
    $html .= "<td nowrap>".format_number($rowRes[$row['l2_id']])."</td>";
    $html .= "<td nowrap>".format_number($row['total_contract_value'])."</td>";
    $html .= "<td nowrap>".format_number( ($rowRes[$row['l2_id']]/$total_revenue) * 100 ,2,2)."</td>";
    $html .= "</tr>";
    $total_thuc_hien += $rowRes[$row['l2_id']];
    $total_hop_dong  += $row['total_contract_value'];
}

$html .= "<tr style = 'font-weight:bold'>";
$html .= "<td colspan='3' nowrap>Tổng cộng</td>";
$html .= "<td nowrap>".format_number($total_thuc_hien)."</td>";
$html .= "<td nowrap>".format_number($total_hop_dong)."</td>";
$html .= "<td nowrap>100%</td>";
$html .= "</tr>";
echo $html;
?>
