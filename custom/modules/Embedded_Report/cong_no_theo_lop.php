<?php
$this->query_list[0] = str_replace('GROUP BY j_class.name','GROUP BY j_class.id, j_class.name',$this->query_list[0]);
$this->query_list[0] = str_replace("IFNULL(j_class.name,'') j_class_name","IFNULL(j_class.id, '') j_class_id, IFNULL(j_class.name,'') j_class_name",$this->query_list[0]);
$this->query_list[0] = str_replace("sum(IFNULL(IFNULL(j_class","(IFNULL(IFNULL(j_class",$this->query_list[0]);
$this->$query_name = $this->query_list[0];
$rows = $GLOBALS['db']->fetchArray($this->query_list[0]);
foreach( $rows as $key => $row){
    $q1 = "SELECT DISTINCT
    SUM(IFNULL(IFNULL(l3.payment_amount, 0) / 1,
    IFNULL(l3.payment_amount, 0))) l3_sum_payment_amount
    FROM
    j_class
    INNER JOIN
    j_studentsituations l1 ON j_class.id = l1.ju_class_id
    AND l1.deleted = 0
    INNER JOIN
    j_payment l2 ON l1.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN
    j_paymentdetail l3 ON l2.id = l3.payment_id AND l3.deleted = 0
    WHERE
    (((l3.payment_no = 1)
    AND (l3.status = 'Paid')
    AND (j_class.id = '{$row['j_class_id']}')))
    AND j_class.deleted = 0
    GROUP BY j_class.id
    ORDER BY j_class.id ASC";
    $sum1 = $GLOBALS['db']->getOne($q1);
    if(empty($sum1)) $sum1 = 0;

    $q0 = "
    SELECT DISTINCT
    SUM(l2.deposit_amount) l3_sum_payment_amount
    FROM
    j_class
    INNER JOIN
    j_studentsituations l1 ON j_class.id = l1.ju_class_id
    AND l1.deleted = 0
    INNER JOIN
    j_payment l2 ON l1.payment_id = l2.id AND l2.deleted = 0
    WHERE
    (((j_class.id = '{$row['j_class_id']}')))
    AND j_class.deleted = 0
    GROUP BY j_class.id";
    $sum0 = $GLOBALS['db']->getOne($q0);
    if(empty($sum0)) $sum0 = 0;

    $sum1 = $sum1 + $sum0;

    $q2 = "SELECT DISTINCT
    SUM(IFNULL(IFNULL(l3.payment_amount, 0) / 1,
    IFNULL(l3.payment_amount, 0))) l3_sum_payment_amount
    FROM
    j_class
    INNER JOIN
    j_studentsituations l1 ON j_class.id = l1.ju_class_id
    AND l1.deleted = 0
    INNER JOIN
    j_payment l2 ON l1.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN
    j_paymentdetail l3 ON l2.id = l3.payment_id AND l3.deleted = 0
    WHERE
    (((l3.payment_no = 2)
    AND (l3.status = 'Paid')
    AND (j_class.id = '{$row['j_class_id']}')))
    AND j_class.deleted = 0
    GROUP BY j_class.id
    ORDER BY j_class.id ASC";
    $sum2 = $GLOBALS['db']->getOne($q2);
    if(empty($sum2)) $sum2 = 0;

    $q3 = "SELECT DISTINCT
    SUM(IFNULL(IFNULL(l3.payment_amount, 0) / 1,
    IFNULL(l3.payment_amount, 0))) l3_sum_payment_amount
    FROM
    j_class
    INNER JOIN
    j_studentsituations l1 ON j_class.id = l1.ju_class_id
    AND l1.deleted = 0
    INNER JOIN
    j_payment l2 ON l1.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN
    j_paymentdetail l3 ON l2.id = l3.payment_id AND l3.deleted = 0
    WHERE
    (((l3.payment_no >= 3)
    AND (l3.status = 'Paid')
    AND (j_class.id = '{$row['j_class_id']}')))
    AND j_class.deleted = 0
    GROUP BY j_class.id
    ORDER BY j_class.id ASC";
    $sum3 = $GLOBALS['db']->getOne($q3);
    if(empty($sum3)) $sum3 = 0;

    $q4 = "SELECT DISTINCT
    SUM(IFNULL(IFNULL(l3.payment_amount, 0) / 1,
    IFNULL(l3.payment_amount, 0))) l3_sum_payment_amount
    FROM
    j_class
    INNER JOIN
    j_studentsituations l1 ON j_class.id = l1.ju_class_id
    AND l1.deleted = 0
    INNER JOIN
    j_payment l2 ON l1.payment_id = l2.id AND l2.deleted = 0
    INNER JOIN
    j_paymentdetail l3 ON l2.id = l3.payment_id AND l3.deleted = 0
    WHERE
    (((l3.status = 'Unpaid')
    AND (j_class.id = '{$row['j_class_id']}')))
    AND j_class.deleted = 0
    GROUP BY j_class.id
    ORDER BY j_class.id ASC";
    $sum4 = $GLOBALS['db']->getOne($q4);
    if(empty($sum4)) $sum4 = 0;

//    $q5 = "SELECT DISTINCT
//    IFNULL(j_class.id, '') j_class_id,
//    IFNULL(j_class.name, '') j_class_name,
//    IFNULL(j_class.class_code, '') j_class_class_code,
//    IFNULL(l1.name, '') l1_name,
//    IFNULL(j_class.aims_id, '') j_class_aims_id,
//    COUNT(DISTINCT l3.id) l3__count,
//    SUM(IFNULL(IFNULL(l2.total_amount, 0) / 1,
//    IFNULL(l2.total_amount, 0))) l2_sum_total_amount
//    FROM
//    j_class
//    INNER JOIN
//    teams l1 ON j_class.team_id = l1.id
//    AND l1.deleted = 0
//    INNER JOIN
//    j_studentsituations l2 ON j_class.id = l2.ju_class_id
//    AND l2.deleted = 0
//    INNER JOIN
//    contacts l3 ON l2.student_id = l3.id AND l3.deleted = 0
//    WHERE
//    (((j_class.id = '{$row['j_class_id']}')
//    AND (l3.type = 'Public')))
//    AND j_class.deleted = 0
//    GROUP BY j_class.id , j_class.name , j_class.class_code , l1.name , j_class.aims_id
//    ORDER BY l1_name ASC";
//    $rs5    = $GLOBALS['db']->query($q5);
//    $sum5   = $GLOBALS['db']->fetchByAssoc($rs5);
//    if(empty($sum5['l3__count'])) $sum5['l3__count'] = 0;
//    if(empty($sum5['l2_sum_total_amount'])) $sum5['l2_sum_total_amount'] = 0;

    $GLOBALS['db']->query("UPDATE j_class SET sum_lan_1=$sum1, sum_lan_2=$sum2, sum_lan_3=$sum3, con_lai=$sum4 WHERE id='{$row['j_class_id']}'");
}
?>
