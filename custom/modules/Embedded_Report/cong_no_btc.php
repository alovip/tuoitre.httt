<?php
$rows    = $GLOBALS['db']->fetchArray($this->query);
$today = date('Y-m-d');
$date2=date_create($today);
foreach( $rows as $key => $row){
    $ext_aging = "aging_report = '', ";
    if($row['j_paymentdetail_status'] == 'Unpaid'){
        $date1  = date_create($row['J_PAYMENTDETAIL_EXPECTD482E5']);
        $diff   = date_diff($date1,$date2);   //AGING
        $aging  = $diff->format("%R%a");
        if($aging > 0){
            $ext_aging = "aging_report = '{$diff->format("%r%a days")}', ";
        }else
             $ext_aging = "aging_report = '0 days', ";
    }
    $ext_completed = '';
    if(array_key_exists('J_PAYMENTDETAIL_PERCEN31D949',$row)){
        $q2 = "SELECT DISTINCT
        IFNULL(payment_no, '') payment_no,
        IFNULL(status, '') status,
        IFNULL(payment_amount, '0') payment_amount
        FROM j_paymentdetail
        WHERE contract_id = '{$row['l2_id']}' AND ((payment_id IS NULL) OR (payment_id = ''))
        AND deleted <> 1
        AND status <> 'Cancelled'
        ORDER BY payment_no";
        $res2 = $GLOBALS['db']->query($q2);
        $paidAmount = 0;
        $unpaidAmount = 0;
        while ($rowPayDtl = $GLOBALS['db']->fetchByAssoc($res2)) {
            if ($rowPayDtl['status'] == "Unpaid")
                $unpaidAmount += $rowPayDtl['payment_amount'];
            else
                $paidAmount += $rowPayDtl['payment_amount'];
        }
        $percent_completed = $paidAmount/$row['l2_total_contract_value'];
        if($percent_completed > 1) $percent_completed = 1;
        $percent_completed = format_number($percent_completed*100)."%"; //% completed

        $ext_completed = "percent_completed = '$percent_completed', ";
    }
    if(!empty($ext_aging) || !empty($ext_completed))
        $GLOBALS['db']->query("UPDATE j_paymentdetail SET $ext_aging $ext_completed sale_type='Not set' WHERE id = '{$row['primaryid']}'");
}
?>
