<?php
// created: 2018-01-21 17:23:55
$subpanel_layout['list_fields'] = array (
  'lesson_number' => 
  array (
    'type' => 'int',
    'default' => true,
    'vname' => 'LBL_NO',
    'width' => '3%',
    'sortable' => false,
  ),
  'till_hour' => 
  array (
    'type' => 'till_hour',
    'vname' => 'LBL_TILL_HOUR',
    'width' => '3%',
    'default' => true,
    'sortable' => false,
  ),
  'name' => 
  array (
    'name' => 'name',
    'vname' => 'LBL_LIST_SUBJECT',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '15%',
    'default' => true,
    'sortable' => false,
  ),
  'session_status' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_SESSION_STATUS',
    'width' => '7%',
    'sortable' => false,
  ),
  'week_date' => 
  array (
    'type' => 'varchar',
    'vname' => 'Weekdate',
    'width' => '7%',
    'default' => true,
    'sortable' => false,
  ),
  'time_start_end' => 
  array (
    'name' => 'time_start_end',
    'vname' => 'Dates/Times',
    'width' => '12%',
    'default' => true,
    'sort_by' => 'date_start',
  ),
  'duration_cal' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_DURATION',
    'width' => '6%',
    'default' => true,
    'sortable' => false,
  ),
  'teacher_cover_id' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_TEACHER_COVER_NAME',
    'id' => 'TEACHER_COVER_ID',
    'width' => '10%',
    'default' => true,
    'target_module' => 'C_Teachers',
    'target_record_key' => 'teacher_cover_id',
  ),
  'teacher_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'vname' => 'LBL_TEACHER_NAME',
    'id' => 'TEACHER_ID',
    'width' => '15%',
    'default' => true,
    'widget_class' => 'SubPanelDetailViewLink',
    'target_module' => 'C_Teachers',
    'target_record_key' => 'teacher_id',
    'sortable' => false,
  ),
  'rate_teacher_n' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_RATE_TEACHER',
    'width' => '2%',
    'default' => true,
    'sortable' => false,
  ),
  'rate_document_n' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_RATE_DOCUMENT',
    'width' => '2%',
    'default' => true,
    'sortable' => false,
  ),
  'rate_logistic_n' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_RATE_LOGISTIC',
    'width' => '2%',
    'default' => true,
    'sortable' => false,
  ),
  'rate_course_n' => 
  array (
    'type' => 'decimal',
    'vname' => 'LBL_RATE_COURSE',
    'width' => '2%',
    'default' => true,
    'sortable' => false,
  ),
  'student_rating' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_STUDENT_RATING',
    'width' => '15%',
    'default' => true,
    'sortable' => false,
  ),
  'subpanel_button' => 
  array (
    'type' => 'varchar',
    'studio' => 'visible',
    'vname' => 'LBL_SUBPANEL_BUTTON',
    'width' => '7%',
    'default' => true,
    'align' => 'center',
  ),
  'type_of_class' => 
  array (
    'type' => 'enum',
    'default' => false,
    'usage' => 'query_only',
  ),
  'recurring_source' => 
  array (
    'usage' => 'query_only',
  ),
  'meeting_type' => 
  array (
    'usage' => 'query_only',
  ),
  'date_start' => 
  array (
    'usage' => 'query_only',
  ),
  'date_end' => 
  array (
    'usage' => 'query_only',
  ),
  'rate_teacher' => 
  array (
    'usage' => 'query_only',
  ),
  'rate_document' => 
  array (
    'usage' => 'query_only',
  ),
  'rate_logistic' => 
  array (
    'usage' => 'query_only',
  ),
  'rate_course' => 
  array (
    'usage' => 'query_only',
  ),
  'ju_class_id' => 
  array (
    'usage' => 'query_only',
  ),
);