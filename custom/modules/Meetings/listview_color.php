<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

class ListviewLogicHookMeetings {
    //Changing color of listview rows according to Session Status - 24/07/2014 - by MTN
    function listviewcolor_Meetings(&$bean, $event, $arguments) {

        global $timedate;

        $date = $timedate->to_db_date($bean->date_end, false);
        $week_date =  date('l',strtotime($date));
        $bean->week_date = $week_date;


        //////////---Apollo Junior -- Add Button Remove By Quyen.Cao----/////////
        if ($_REQUEST['module']=='Leads' || $_REQUEST['module']=='Contacts'){
            $bean->custom_button = '
            <div style="display: inline-flex;">
            <input type="button" class="remove_demo" class="button" id="'.$bean->id.'" value="Remove">
            </div>';
        }
        //Count Attended and number of student
        if($bean->meeting_type == 'Demo' || $bean->meeting_type == 'Placement Test'){
            if($_REQUEST['module'] == 'Meetings'){
                $now_int    = strtotime($timedate->nowDb());
                $start_int  = strtotime($timedate->to_db($bean->date_start));
                if($now_int >= $start_int){
                    $q1 = "SELECT DISTINCT
                    IFNULL(COUNT(DISTINCT j_ptresult.id), 0) j_ptresult__count
                    FROM
                    j_ptresult
                    INNER JOIN
                    meetings_j_ptresult_1_c l1_1 ON j_ptresult.id = l1_1.meetings_j_ptresult_1j_ptresult_idb
                    AND l1_1.deleted = 0
                    INNER JOIN
                    meetings l1 ON l1.id = l1_1.meetings_j_ptresult_1meetings_ida
                    AND l1.deleted = 0
                    WHERE
                    (((l1.id = '{$bean->id}')
                    AND ((j_ptresult.attended LIKE 'on'
                    OR j_ptresult.attended = '1'))))
                    AND j_ptresult.deleted = 0
                    GROUP BY l1.id";
                    $count_attented = $GLOBALS['db']->getOne($q1);
                }
                if(empty($count_attented)) $count_attented = 0;


                $q2 = "SELECT DISTINCT
                IFNULL(COUNT(DISTINCT j_ptresult.id), 0) j_ptresult__count
                FROM
                j_ptresult
                INNER JOIN
                meetings_j_ptresult_1_c l1_1 ON j_ptresult.id = l1_1.meetings_j_ptresult_1j_ptresult_idb
                AND l1_1.deleted = 0
                INNER JOIN
                meetings l1 ON l1.id = l1_1.meetings_j_ptresult_1meetings_ida
                AND l1.deleted = 0
                WHERE
                (((l1.id = '{$bean->id}')
                AND ((COALESCE(LENGTH(j_ptresult.attended), 0) > 0))))
                AND j_ptresult.deleted = 0
                GROUP BY l1.id";
                $count_student = $GLOBALS['db']->getOne($q2);
                if(empty($count_student)) $count_student = 0;
                $bean->attended             = $count_attented;
                $bean->number_of_student    = $count_student;
            }
        }
        elseif($bean->meeting_type == 'Session'){
            if(!empty($bean->teacher_cover_id)){
                $name = $GLOBALS['db']->getOne("SELECT full_teacher_name FROM c_teachers WHERE id='{$bean->teacher_cover_id}'");
                $bean->teacher_cover_id = "<a href='index.php?module=J_Class&return_module=J_Class&action=DetailView&record={$bean->teacher_cover_id}'>$name</a>";
            }
            //$q11 = "SELECT j_class.id, j_class.kind_of_course, j_class.end_date, COUNT(mt.id) number_of_ss FROM j_class
            //INNER JOIN meetings mt ON mt.ju_class_id = j_class.id AND mt.deleted = 0 AND mt.session_status <> 'Cancelled'
            //WHERE j_class.id = '{$bean->ju_class_id}'";
            //$rs11= $GLOBALS['db']->query($q11);
            //$row11 = $GLOBALS['db']->fetchByAssoc($rs11);
            //$ss_date = $timedate->to_db_date($bean->date_start);
            //if(($row11['kind_of_course'] == 'Future Bankers' || ($row11['kind_of_course'] != 'Future Bankers' && $ss_date == $row11['end_date'] && $row11['number_of_ss'] == $bean->lesson_number)) && $bean->session_status != 'Cancelled' ){
            //$bean->rate_teacher = '<input type="text" meeting_id = "'.$bean->id.'" class="rate_sqm" name="rate_teacher" size="3" title="'.translate('LBL_RATE_TEACHER').'">';
            $bean->rate_teacher_n   =  '<input type="text" value="'.format_number($bean->rate_teacher,2,2).'" meeting_id = "'.$bean->id.'" class="rate_sqm" name="rate_teacher" size="3" title="'.translate('Meetings','LBL_RATE_TEACHER').'">';
            $bean->rate_document_n  =  '<input type="text" value="'.format_number($bean->rate_document,2,2).'" meeting_id = "'.$bean->id.'" class="rate_sqm" name="rate_document" size="3" title="'.translate('Meetings','LBL_RATE_DOCUMENT').'">';
            $bean->rate_logistic_n  =  '<input type="text" value="'.format_number($bean->rate_logistic,2,2).'" meeting_id = "'.$bean->id.'" class="rate_sqm" name="rate_logistic" size="3" title="'.translate('Meetings','LBL_RATE_LOGISTIC').'">';
            $bean->rate_course_n    =  '<input type="text" value="'.format_number($bean->rate_course,2,2).'" meeting_id = "'.$bean->id.'" class="rate_sqm" name="rate_course" size="3" title="'.translate('Meetings','LBL_RATE_COURSE').'">';

            $bean->student_rating   =  '<input type="text" value="'.$bean->student_rating.'" meeting_id = "'.$bean->id.'" class="rate_comment" name="student_rating" size="23" title="'.translate('Meetings','LBL_STUDENT_RATING').'">';
            //}
        }

        //Add button delete Session, Cover Session, Make-up Session on Subpanel
        require_once('custom/include/_helper/class_utils.php');
        if (checkDataLockDate($bean->date_end)) {
            if($bean->type_of_class == 'Junior'){
                if($bean->session_status != 'Cancelled')
                    $bean->subpanel_button = "<div style=\"display: inline-flex;\">
                    <input type=\"button\" class=\"button primary btn_cancel_session\" id=\"{$bean->id}\"
                    value=\"{$GLOBALS['mod_strings']['LBL_BTN_CANCEL']}\" onclick=\"cancelSession($(this));\"></input></div>";
                else{ //redmine#27 ko phai admin ko hien nut xoa
                    if($GLOBALS['current_user']->isAdmin())
                        $bean->subpanel_button = "<div style=\"display: inline-flex;\">
                        <input type=\"button\" class=\"button btn_deleted_session\" id=\"{$bean->id}\"
                        value=\"{$GLOBALS['app_strings']['LBL_DELETE_BUTTON']}\" onclick=\"deleteSession($(this));\"></input></div>";
                    else $bean->subpanel_button = '';
                }
            }else{
                $bean->subpanel_button = '<div style="display: inline-flex;">
                <input type="button" class="button" name="make_up_button" id="'.$bean->id.'" value="'.$GLOBALS['mod_strings']['LBL_MAKE_UP_BUTTON'].'">&nbsp
                <input type="button" class="button" name="cover_button" id="'.$bean->id.'" value="'.$GLOBALS['mod_strings']['LBL_COVER_BUTTON'].'">&nbsp
                <input type="button" class="button primary" name = "delete_button" id="'.$bean->id.'" value="'.$GLOBALS['mod_strings']['LBL_DELETE_BUTTON'].'"></div>';
            }
        }else $bean->subpanel_button = '';
        //Colorzide
        $date_start   = strtotime($timedate->to_db($bean->date_start));
        $date_end     = strtotime($timedate->to_db($bean->date_end));
        $now          = strtotime($timedate->nowDb());
        if($bean->session_status == 'Make-up') {
            $bean->session_status = "<span class='textbg_violet'>Make-up</span>";
        } elseif($bean->session_status == 'Cancelled') {
            $bean->session_status = "<span class='textbg_black'>Cancelled</span>";
        }elseif ( $now < $date_start) {
            $bean->session_status = "<span class='textbg_orange'>Not started</span>";
        } elseif ($now >= $date_start && $now <= $date_end){
            $bean->session_status = "<span class='textbg_bluelight'>In progress</span>";
        } elseif ($now > $date_end){
            $bean->session_status = "<span class='textbg_green'>Finished</span>";
        }
    }
}
?>