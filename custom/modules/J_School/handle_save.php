<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
class logicSchool {
    public function handleSave(&$bean, $event, $arguments){
        global $current_user;
        $bean->assigned_user_id	=	$current_user->id;
        //save school name
        $type_list = $GLOBALS['app_list_strings']['level_school_list'];
        if(!empty($bean->name)){
            foreach ($type_list as $findme){
                $bean->name = str_replace($findme , "" , $bean->name);
            }
            $bean->name = trim($bean->name);
            if(empty($bean->name) || $bean->name == 'Khong Biet' || $bean->name == 'na' || $bean->name == 'n/a')
                $bean->name = 'Chưa cập nhật';
        }
    }

    function listViewColorSchool(&$bean, $event, $arguments){
        $bean->name = $bean->name;
        if(!empty($bean->address_address_street)){
            $bean->name .= " ({$bean->address_address_street})";
        }
    }
}
?>