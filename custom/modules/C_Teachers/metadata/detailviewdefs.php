<?php
$module_name = 'C_Teachers';
$viewdefs[$module_name] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                ),
            ),
            'includes' =>
            array (
                1 =>
                array (
                    'file' => 'custom/include/DateRange/moment.min.js',
                ),
                2 =>
                array (
                    'file' => 'custom/include/DateRange/jquery.daterangepicker.js',
                ),
                3 =>
                array (
                    'file' => 'custom/modules/C_Teachers/js/detailview.js',
                ),
            ),
            'useTabs' => false,
            'maxColumns' => '2',
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_MORE_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_OTHER_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'teacher_id',
                        'label' => 'LBL_TEACHER_ID',
                        'customCode' => '{$TEACHER_CODE}',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'studio' => 'visible',
                        'label' => 'LBL_STATUS',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_teacher_name',
                        'label' => 'LBL_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'picture',
                        'comment' => 'Picture file',
                        'label' => 'LBL_PICTURE_FILE',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'dob',
                        'label' => 'LBL_DOB',
                    ),
                    1 =>
                    array (
                        'name' => 'nationality',
                        'label' => 'LBL_NATIONALITY',
                    ),
                ),
                3 =>
                array (
                    0 => 'phone_mobile',
                    1 =>
                    array (
                        'name' => 'phone_home',
                        'label' => 'LBL_HOME_PHONE',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'experience',
                        'studio' => 'visible',
                        'label' => 'LBL_EXPERIENCE',
                    ),
                    1 =>
                    array (
                        'name' => 'type',
                        'studio' => 'visible',
                        'label' => 'LBL_TYPE',
                    ),
                ),
                5 =>
                array (
                    0 => 'email1',
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'label' => 'LBL_PRIMARY_ADDRESS',
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                        ),
                    ),
                ),
                6 =>
                array (
                    0 => 'description',
                ),
            ),
            'LBL_MORE_INFORMATION' =>
            array (
                0 =>
                array (
                    0 => 'title',
                    1 => 'department',
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'bank',
                        'label' => 'LBL_BANK',
                    ),
                    1 =>
                    array (
                        'name' => 'identity_number',
                        'label' => 'LBL_INDENTITY_NUMBER',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'tax_code',
                        'label' => 'LBL_TAX_CODE',
                    ),
                    1 =>
                    array (
                        'name' => 'salary_manday',
                        'label' => 'LBL_SALARY_MANDAY',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'mang',
                        'label' => 'LBL_MANG',
                    ),
                    1 =>
                    array (
                        'name' => 'salary_per_coach',
                        'label' => 'LBL_SALARY_PER_COACH',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'salary_increase',
                        'label' => 'LBL_SALARY_INCREASE',
                    ),
                    1 =>
                    array (
                        'name' => 'salary_fb',
                        'label' => 'LBL_SALARY_FB',
                    ),
                ),
            ),
            'LBL_OTHER_INFORMATION' =>
            array (
                0 => array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                    ),
                    1 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                1 => array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
