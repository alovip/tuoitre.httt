<?php
$module_name = 'C_Teachers';
$viewdefs[$module_name] =
array (
  'EditView' =>
  array (
    'templateMeta' =>
    array (
      'form' =>
      array (
        'hidden' =>
        array (
          1 => '<input type="hidden" name="teacher_type" id="teacher_type" value="{$fields.teacher_type.value}">',
        ),
      ),
      'maxColumns' => '2',
      'javascript' => '
            {sugar_getscript file="custom/modules/C_Teachers/js/editview.js"}',
      'useTabs' => false,
      'widths' =>
      array (
        0 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 =>
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'tabDefs' =>
      array (
        'LBL_CONTACT_INFORMATION' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_MORE_INFORMATION' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_OTHER_INFORMATION' =>
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' =>
    array (
      'lbl_contact_information' =>
      array (
        0 =>
        array (
          0 =>
          array (
            'name' => 'teacher_id',
            'label' => 'LBL_TEACHER_ID',
            'customCode' => '<input type="text" class="input_readonly" name="teacher_idd" id="teacher_id" maxlength="255" value="{$fields.teacher_id.value}" title="{$MOD.LBL_NO_CONTRACT}" size="30" readonly>',


          ),
          1 =>
          array (
            'name' => 'status',
            'studio' => 'visible',
            'label' => 'LBL_STATUS',
          ),
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'name',
            'customLabel' => '{$MOD.LBL_NAME} <span class="required">*</span>',
            'customCode' => '
                        <input name="last_name" id="last_name" placeholder="{$MOD.LBL_LAST_NAME|replace:\':\':\'\'}" style="width:120px !important" size="15" type="text" value="{$fields.last_name.value}">
                        &nbsp;<input name="first_name" id="first_name" placeholder="{$MOD.LBL_FIRST_NAME|replace:\':\':\'\'}" size="20" type="text"  value="{$fields.first_name.value}"></br>
                        <span style=" color: #A99A9A; font-style: italic;">Ex: Amanda Natalie Costigan  </br> Last Name: Natalie Costigan </br> First Name:  Amanda </span>',
          ),
          1 =>
          array (
            'name' => 'picture',
            'comment' => 'Picture file',
            'label' => 'LBL_PICTURE_FILE',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'dob',
            'label' => 'LBL_DOB',
          ),
          1 =>
          array (
            'name' => 'nationality',
            'label' => 'LBL_NATIONALITY',
          ),
        ),
        3 =>
        array (
          0 => 'phone_mobile',
          1 => 'phone_home',
        ),
        4 =>
        array (
          0 =>
          array (
            'name' => 'experience',
            'studio' => 'visible',
            'label' => 'LBL_EXPERIENCE',
          ),
          1 =>
          array (
            'name' => 'type',
            'studio' => 'visible',
            'label' => 'LBL_TYPE',
          ),
        ),
        5 =>
        array (
          0 => 'email1',
          1 =>
          array (
            'name' => 'primary_address_street',
            'hideLabel' => true,
            'type' => 'address',
            'displayParams' =>
            array (
              'key' => 'primary',
              'rows' => 2,
              'cols' => 30,
              'maxlength' => 150,
            ),
          ),
        ),
        6 =>
        array (
          0 =>
          array (
            'name' => 'description',
            'displayParams' =>
            array (
              'rows' => 2,
              'cols' => 60,
            ),
          ),
        ),
      ),
      'LBL_MORE_INFORMATION' =>
      array (
        0 =>
        array (
          0 => 'title',
          1 => 'department',
        ),
        1 =>
        array (
          0 =>
          array (
            'name' => 'bank',
            'label' => 'LBL_BANK',
          ),
          1 =>
          array (
            'name' => 'identity_number',
            'label' => 'LBL_INDENTITY_NUMBER',
          ),
        ),
        2 =>
        array (
          0 =>
          array (
            'name' => 'tax_code',
            'label' => 'LBL_TAX_CODE',
          ),
          1 =>
          array (
            'name' => 'salary_manday',
            'label' => 'LBL_SALARY_MANDAY',
          ),
        ),
        3 =>
        array (
          0 =>
          array (
            'name' => 'mang',
            'label' => 'LBL_MANG',
          ),
          1 =>
          array (
            'name' => 'salary_per_coach',
            'label' => 'LBL_SALARY_PER_COACH',
          ),
        ),
        4 =>
        array (
          0 =>
          array (
            'name' => 'salary_increase',
            'label' => 'LBL_SALARY_INCREASE',
          ),
          1 =>
          array (
            'name' => 'salary_fb',
            'label' => 'LBL_SALARY_FB',
          ),
        ),
      ),
      'LBL_OTHER_INFORMATION' =>
      array (
        0 =>
        array (
          0 => 'assigned_user_name',
          1 =>
          array (
            'name' => 'team_name',
            'displayParams' =>
            array (
              'display' => true,
            ),
          ),
        ),
      ),
    ),
  ),
);
