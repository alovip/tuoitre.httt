<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
/**
*
* LICENSE: The contents of this file are subject to the license agreement ("License") which is included
* in the installation package (LICENSE.txt). By installing or using this file, you have unconditionally
* agreed to the terms and conditions of the License, and you may not use this file except in compliance
* with the License.
*
* @author     Original Author Biztech Co.
*/

require_once('modules/Contacts/views/view.detail.php');

class CustomContactsViewDetail extends ContactsViewDetail {

    /**
    * @see SugarView::display()
    *
    * We are overridding the display method to manipulate the portal information.
    * If portal is not enabled then don't show the portal fields.
    */
    function display() {
        global $current_user, $mod_strings;
        if(ACLController::checkAccess('bc_survey', 'view', true)){
            // Customize Survey
            echo '<link rel="stylesheet" type="text/css" href="custom/include/css/survey_css/jquery.datetimepicker.css">';
            echo '<link rel="stylesheet" type="text/css" href="custom/include/css/survey_css/survey.css">';
            echo '<script type="text/javascript" src="custom/include/js/survey_js/custom_code.js"></script>';
            require_once('custom/include/modules/Administration/plugin.php');
            $checkSurveySubscription = validateSurveySubscription();
            if (!$checkSurveySubscription['success']) {

            } else {
                $record_id = (!empty($_REQUEST['record'])) ? $_REQUEST['record'] : $this->bean->id;
                $module_name = (!empty($_REQUEST['module'])) ? $_REQUEST['module'] : $this->module;
                $send_survey = "<input  type='button' id='send_survey' onclick=\"create_SendSurveydiv('{$record_id}','{$module_name}');\" value='Send Survey'>";
                $send_poll   = "<input  type='button' id='send_poll' onclick=\"create_SendPolldiv('{$record_id}','{$module_name}');\" value='Send Poll'>";
                $this->ss->assign('send_survey', $send_survey);
                $this->ss->assign('send_poll', $send_poll);
            }
            //End Customize Survey
        }
        $this->ss->assign("PORTAL_ENABLED", true);


        $html_bt = '';
        $html_bt .= '<input style="margin-left: 5px;" id="btn_view_change_log" title="View Change Log" class="button" onclick="open_popup(\'Audit\', \'600\', \'400\', \'&record='.$this->bean->id.'&module_name=Contacts\', true, false,  { \'call_back_function\':\'set_return\',\'form_name\':\'EditView\',\'field_to_name_array\':[] } ); return false;" type="button" value="View Change Log">';

        $this->ss->assign('CUSTOM_BUTTON',$html_bt);

        $team_type = getTeamType($this->bean->team_id);
        $this->ss->assign('team_type',$team_type);

        //Total Payment - Junior
        $sql1 = "SELECT DISTINCT
        COUNT(j_payment.id) j_payment__allcount
        FROM
        j_payment
        INNER JOIN
        contacts_j_payment_1_c l1_1 ON j_payment.id = l1_1.contacts_j_payment_1j_payment_idb
        AND l1_1.deleted = 0
        INNER JOIN
        contacts l1 ON l1.id = l1_1.contacts_j_payment_1contacts_ida
        AND l1.deleted = 0
        WHERE
        (((l1.id = '{$this->bean->id}')))
        AND j_payment.deleted = 0";
        $bt_delete = '<input title="Delete" accesskey="d" class="button" onclick="var _form = document.getElementById(\'formDetailView\'); _form.return_module.value=\'Contacts\'; _form.return_action.value=\'ListView\'; _form.action.value=\'Delete\'; if(confirm(\'Are you sure you want to delete this record?\')) SUGAR.ajaxUI.submitForm(_form);" type="submit" name="Delete" value="Delete" id="delete_button">';
        $this->ss->assign('DELETE',$bt_delete);
        //status color
        $color = '';
        if($this->bean->contact_status == "Waiting for class")    $color .= ' <span class="textbg_crimson"><b>'.$this->bean->contact_status.'<b></span>';
        else if($this->bean->contact_status == "In Progress")      $color .= ' <span class="textbg_blue"><b>'.$this->bean->contact_status.'<b></span>';
            else if($this->bean->contact_status == "Delayed")            $color .= ' <span class="textbg_black"><b>'.$this->bean->contact_status.'<b></span>';
                else if($this->bean->contact_status == "OutStanding")     $color .= ' <span class="textbg_orange"><b>'.$this->bean->contact_status.'<b></span>';
                    else if($this->bean->contact_status == "Finished")     $color .= ' <span class="textbg_green"><b>'.$this->bean->contact_status.'<b></span>';
                        $this->ss->assign('COLOR',$color);

        //Custom Quickedit
        if(ACLController::checkAccess('J_Payment', 'import', true)){
            $user_list = $GLOBALS['db']->fetchArray("SELECT DISTINCT IFNULL(users.id,'') primaryid ,CONCAT(IFNULL(users.last_name,''),' ',IFNULL(users.first_name,'')) users_full_name FROM users INNER JOIN teams l1 ON users.default_team=l1.id AND l1.deleted=0 WHERE (((l1.id='{$this->bean->team_id}' ) AND ((users.is_admin is null OR users.is_admin='0') ) AND (users.status = 'Active' ) AND (users.for_portal_only = '0' ) AND (users.portal_only = '0' ) )) AND users.deleted=0 ORDER BY users.last_name, users.first_name ASC");
            $user_arr = array($current_user->id => $current_user->name);
            foreach($user_list as $key => $user)
                $user_arr[$user['primaryid']] = $user['users_full_name'];

            $assigned_user_idQ = '<img id="loading_assigned_user_id" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_assigned_user_id"><label id="label_assigned_user_id">'.$this->bean->assigned_user_name.'</label>&nbsp&nbsp<a id="btnedit_assigned_user_id" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_assigned_user_id" style="display: none;"><select id="value_assigned_user_id">'.get_select_options($user_arr, $this->bean->assigned_user_id).'</select>
            &nbsp&nbsp<a title="Save" id="btnsave_assigned_user_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_assigned_user_id"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';
        }else
            $assigned_user_idQ    = '<label id="label_assigned_user_id">'.$this->bean->assigned_user_name.'</label>';

        if($team_type == 'Adult'){
            $show_add_week = '';
            if($this->bean->show_add_week == '1')
                $show_add_week = 'checked';
            $additional_weekQ = '<img id="loading_additional_week" src=\'custom/include/images/fb_loading.gif\' style=\'width:15px; height:15px; display:none;\'/>
            <div id="panel_1_additional_week"><label id="label_additional_week">'.$this->bean->additional_week.' &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label> Is Activated ? <input type="checkbox" disabled id="label_show_add_week" '.$show_add_week.' value="'.$this->bean->show_add_week.'">&nbsp&nbsp<a id="btnedit_additional_week" title="Edit" title="Admin Edit"><i style="font-size: 20px;cursor: pointer;" class="icon icon-edit"></i></a></div>
            <div id="panel_2_additional_week" style="display: none;">
            <input type="text" name="value_additional_week" id="value_additional_week" value="'.$this->bean->additional_week.'" size="4" maxlength="10">
            Is Activated ?
            <input type="checkbox" id="value_show_add_week" name="value_show_add_week" '.$show_add_week.' value="'.$this->bean->show_add_week.'">
            &nbsp&nbsp<a title="Save" id="btnsave_additional_week"><i style="font-size: 20px;cursor: pointer;" class="icon icon-download-alt"></i></a> <a title="Cancel" id="btncancel_additional_week"><i style="font-size: 20px;cursor: pointer;" class="icon icon-remove"></i></a></div>';
        }else{
            $additional_weekQ = '';
        }

        $this->ss->assign('additional_weekQ',$additional_weekQ);

        $this->ss->assign('assigned_user_idQ',$assigned_user_idQ);

        //Generate School
        if(!empty($this->bean->j_school_contacts_1j_school_ida)){
            $school = BeanFactory::getBean('J_School',$this->bean->j_school_contacts_1j_school_ida);
            $school->name = $school->name;
            if(!empty($school->address_address_street)){
                $school->name .= " ({$school->address_address_street})";
            }
            $this->bean->j_school_contacts_1_name = $school->name;
        }
        //Load Contact
        $contact = BeanFactory::getBean('C_Contacts', $this->bean->c_contacts_contacts_1c_contacts_ida);
        $this->bean->contact_mobile = $contact->mobile_phone;
        $this->bean->contact_position = $contact->position;

        parent::display();
    }

    function _displaySubPanels(){
        require_once ('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);

        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['c_classes_contacts_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['opportunities']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_payments_2']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_refunds_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_payments_1']);

        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_payments_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_payments_2']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['bc_survey_submission_contacts']);

        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_invoices_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['documents']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_contacts_1']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['quotes']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['cases']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['leads_contacts_1']);

        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['leads']['top_buttons'][0]);//hiding create
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['leads']['top_buttons'][1]);//hiding select
        //hide activities and history
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['campaigns']);
        unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['contacts_c_invoices_1']);

        if(!ACLController::checkAccess('bc_survey', 'view', true)){
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['bc_survey_contacts']);
            unset($subpanel->subpanel_definitions->layout_defs['subpanel_setup']['bc_survey_submission_contacts']);
        }
        echo $subpanel->display();
    }

}
