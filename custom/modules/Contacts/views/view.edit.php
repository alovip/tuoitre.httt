<?php

class ContactsViewEdit extends ViewEdit {

    function ContactsViewEdit(){
        parent::ViewEdit();
    }

    function preDisplay(){
        parent::preDisplay();
    }
    public function display()
    {
        global $beanFiles, $current_user;
        //Handle Convert Lead to Student
        $is_lead_convert = false;
        if(!empty($_REQUEST['return_id']) && $_REQUEST['return_module'] == 'Leads'){
            $lead = BeanFactory::getBean('Leads', $_REQUEST['return_id']);
            foreach ($lead->field_defs as $keyField => $aFieldName)
                $this->bean->$keyField = $lead->$keyField;

            $this->bean->j_school_contacts_1_name = $lead->j_school_leads_1_name;
            $this->bean->j_school_contacts_1j_school_ida = $lead->j_school_leads_1j_school_ida;

            $this->bean->c_contacts_contacts_1c_contacts_ida    = $lead->c_contacts_leads_1c_contacts_ida;
            $this->bean->c_contacts_contacts_1_name             = $lead->c_contacts_leads_1_name;

            $this->bean->id = '';
            $this->bean->assigned_user_id = $lead->assigned_user_id;
            $this->bean->assigned_user_name = get_assigned_user_name($this->bean->assigned_user_id);
            $is_lead_convert = true;
        }
        $this->ss->assign('is_lead_convert', $is_lead_convert);

        if(!empty($this->bean->campaign_id) && empty($this->bean->campaign_name))
            $this->bean->campaign_id = '';

        if(!empty($this->bean->j_school_contacts_1j_school_ida) && empty($this->bean->j_school_contacts_1_name))
            $this->bean->j_school_contacts_1j_school_ida = '';

        if($_POST['isDuplicate'] == 'true'){
            $this->bean->contact_id         = translate('LBL_AUTO_GENERATE','Accounts');
            $status .= '<label>Waiting for class</label>' ;
        }else{
            if(empty($this->bean->id)){
                $this->bean->contact_id     = translate('LBL_AUTO_GENERATE','Accounts');
                $status .= '<label>Waiting for class</label>' ;
            }else{
                $status .= '<label>'.$this->bean->contact_status.'</label>' ;
            }
        }

        $this->ss->assign('assigned_user_id_2', $this->bean->assigned_user_id);
        $this->ss->assign('birthdate_2', $this->bean->birthdate);
        $this->ss->assign('first_name_2', $this->bean->first_name);
        $this->ss->assign('last_name_2', $this->bean->last_name);
        $this->ss->assign('phone_mobile_2', $this->bean->phone_mobile);

        //generate Lead Source
        $html_source = '<select title="'.translate('LBL_LEAD_SOURCE').'" style="width: 40%;" name="lead_source" id="lead_source"><option value="">-none-</option>';
        $html_source .= '<optgroup label="Online">';
        foreach($GLOBALS['app_list_strings']['online_source_list'] as $key => $value){
            $sel = ($this->bean->lead_source == $key) ? 'selected' : '';
            $html_source .= '<option '.$sel.' value="'.$key.'">'.$value.'</option>';
        }
        $html_source .= '</optgroup>';
        $html_source .= '<optgroup label="Offline">';
        foreach($GLOBALS['app_list_strings']['offline_source_list'] as $key => $value){
            $sel = ($this->bean->lead_source == $key) ? 'selected' : '';
            $html_source .= '<option '.$sel.' value="'.$key.'">'.$value.'</option>';
        }
        $html_source .= '</optgroup></select>';
        $html_source .= '<select title="'.translate('LBL_ACTIVITY').'" style="width: 30%; margin-left: 4px;" name="activity" id="activity">';
        $html_source .= get_select_options_with_id($GLOBALS['app_list_strings']['activity_source_list'], $this->bean->activity);
        $html_source .= '</select>';
        if(ACLController::checkAccess('J_Marketingplan', 'import', true))
            $this->ss->assign('is_role_mkt', '1');
        else
            $this->ss->assign('is_role_mkt', '1');

        $this->ss->assign('lead_source', $html_source);
        //End: Generate Lead Source

        $this->ss->assign('team_type',$_type);
        $this->ss->assign('html_koc',$html_koc);
        $this->ss->assign('STATUS', $status);
        $this->ss->assign('phone_mobile', $this->bean->phone_mobile);
        $this->ss->assign('full_student_name', $this->bean->last_name.' '.$this->bean->first_name);

        //Navigate Form Base convert Lead -> Student
        if (!empty($_REQUEST['return_id']) &&  $_REQUEST['return_module'] =='Leads'){
            $this->ss->assign('lead_id', $_REQUEST['return_id']);
            $_REQUEST['return_module'] 	= 'Contacts';
            $_REQUEST['return_id'] 		= '';
        }


        //Load Contact
        $contact = BeanFactory::getBean('C_Contacts', $this->bean->c_contacts_contacts_1c_contacts_ida);
        $this->bean->contact_mobile = $contact->mobile_phone;
        $this->bean->contact_position = $contact->position;
        parent::display();
    }
}