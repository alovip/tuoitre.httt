<?php
$viewdefs['Contacts'] =
array (
    'EditView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'enctype' => 'multipart/form-data',
                'hidden' =>array (
                    0 => '<input type="hidden" name="opportunity_id" value="{$smarty.request.opportunity_id}">',
                    1 => '<input type="hidden" name="lead_id" value="{$lead_id}">',
                    2 => '<input type="hidden" name="case_id" value="{$smarty.request.case_id}">',
                    3 => '<input type="hidden" name="bug_id" value="{$smarty.request.bug_id}">',
                    4 => '<input type="hidden" name="email_id" value="{$smarty.request.email_id}">',
                    5 => '<input type="hidden" name="inbound_email_id" value="{$smarty.request.inbound_email_id}">',
                    6 => '<input type="hidden" name="assigned_user_id_2" value="{$assigned_user_id_2}">',
                    7 => '<input type="hidden" id="team_type" name value="{$team_type}">',
                    8 => '<input type="hidden" name="birthdate_2" value="{$birthdate_2}">',
                    9 => '<input type="hidden" name="last_name_2" value="{$last_name_2}">',
                    10 => '<input type="hidden" name="first_name_2" value="{$first_name_2}">',
                    11 => '<input type="hidden" name="phone_mobile_2" value="{$phone_mobile_2}">',
                ),
            ),
            'maxColumns' => '2',
            'javascript' => '{sugar_getscript file="custom/modules/Contacts/js/editview.js"}
            {sugar_getscript file="custom/include/javascripts/Multifield/jquery.multifield.js"}',
            'useTabs' => true,
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_COMPANY' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'contact_id',
                        'label' => 'LBL_CONTACT_ID',
                        'customCode' => '<input type="text" class="input_readonly" name="contact_idd" id="contact_id" maxlength="255" value="{$fields.contact_id.value}" title="{$MOD.LBL_CONTACT_ID}" size="30" readonly>',
                    ),
                    1 => 'type'
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_student_name',
                        'customLabel' => '{$MOD.LBL_NAME} <span class="required">*</span>',
                        'customCode' => '
                        {if $fields.full_student_name.acl > 1 || $is_lead_convert}
                        <table width="100%" style="padding:0px!important;width: 300px;">
                        <tbody><tr>
                        <td style="padding: 0px !important;" width = "60%"><input name="last_name" id="last_name" placeholder="{$MOD.LBL_LAST_NAME|replace:\':\':\'\'}" style="margin-right: 3px;" size="20" type="text"  value="{$fields.last_name.value}"></td>
                        <td style="padding: 0px !important;" width="40%"><input name="first_name" id="first_name" placeholder="{$MOD.LBL_FIRST_NAME|replace:\':\':\'\'}" style="width:120px !important; margin-right: 3px;" size="15" type="text" value="{$fields.first_name.value}"></td>
                        </tr>
                        <tr><td colspan="2"><span style=" color: #A99A9A; font-style: italic;"> Bùi Vũ Thanh An | Họ: Bùi Vũ Thanh - Tên:  An </span></td></tr>
                        </tbody>
                        </table><div id = "dialogDuplicationLocated"></div>
                        {else}
                        <span id="full_student_name">
                        {$full_student_name}
                        </span>
                        {/if}',
                    ),
                    1 => 'picture',
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'gender',
                        'studio' => 'visible',
                        'label' => 'LBL_GENDER',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'birthdate',
                        'customLabel' => '{$MOD.LBL_BIRTHDATE}',
                        'comment' => 'The birthdate of the contact',
                        'customCode' => '
                        {if $fields.birthdate.acl > 1 || $is_lead_convert}
                        <span class="dateTime"><input class="date_input" autocomplete="off" type="text" name="birthdate" id="birthdate" value="{$fields.birthdate.value}" title="{$MOD.LBL_BIRTHDATE}" tabindex="0" size="11" maxlength="10" style="width: 110px !important;"></span><img src="themes/OnlineCRM-Blue/images/jscalendar.png" alt="Enter Date" style="position:relative; top:6px; padding-left: 4px;" border="0" id="birthdate_trigger"></span>
                        {literal}
                        <script type="text/javascript">
                        Calendar.setup ({
                        inputField : "birthdate",
                        ifFormat : cal_date_format,
                        daFormat : cal_date_format,
                        button : "birthdate_trigger",
                        singleClick : true,
                        dateStr : "{$fields.birthdate.value}",
                        startWeekday: 0,
                        step : 1,
                        weekNumbers:false
                        }
                        );
                        </script>
                        {/literal}
                        {else}
                        <span id="birthdate">
                        {$fields.birthdate.value}
                        </span>
                        {/if}
                        ',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'email1',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL_ADDRESS',
                    ),
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'hideLabel' => true,
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                            'rows' => 2,
                            'cols' => 30,
                            'maxlength' => 150,
                        ),
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'phone_mobile',
                        'customLabel' => '{$MOD.LBL_MOBILE_PHONE}',
                        'customCode' => '{if $fields.phone_mobile.acl > 1 || $is_lead_convert}
                        <span id="phone_mobile_span">
                        {$fields.phone_mobile.value}
                        </span>
                        {else}
                        <span id="phone_mobile_span">
                        {$phone_mobile}
                        </span>
                        {/if}',
                    ),
                    1 =>
                    array (
                        'name' => 'other_mobile',
                    ),
                ),
                5 =>
                array (
                    0 => 'facebook',
                    1 =>
                    array (
                        'name' => 'phone_other',
                        'comment' => 'Other phone number for the contact',
                        'label' => 'LBL_OTHER_PHONE',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'description',
                        'displayParams' =>
                        array (
                            'rows' => 4,
                            'cols' => 55,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'do_not_call',
                        'comment' => 'An indicator of whether contact can be called',
                        'label' => 'LBL_DO_NOT_CALL',
                    ),
                ),
            ),
            'lbl_panel_company' =>
            array (
                0 =>
                array (
                    0 => 'identity_number',
                    1 =>
                    array (
                        'name' => 'alt_address_street',
                        'label' => 'LBL_ALTERNATE_ADDRESS',
                        'customCode' => '<textarea rows="2" cols="30" name="alt_address_street" >{$fields.alt_address_street.value}</textarea>',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_date',
                        'label' => 'LBL_INDENTITY_DATE',
                    ),
                    1 =>
                    array (
                        'name' => 'weight',
                        'label' => 'LBL_WEIGHT',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_location',
                        'label' => 'LBL_INDENTITY_LOCATION',
                    ),
                    1 =>
                    array (
                        'name' => 'height',
                        'label' => 'LBL_HEIGHT',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_school_contacts_1_name',
                        'label' => 'LBL_J_SCHOOL_CONTACTS_1_FROM_J_SCHOOL_TITLE',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_year',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_YEAR',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'graduated_major',
                        'label' => 'LBL_GRADUATED_MAJOR',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_rate',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_RATE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'account_name',
                        'displayParams' =>
                        array (
                            'field_to_name_array' =>
                            array (
                                'id' => 'account_id',
                                'name' => 'account_name',
                                'phone_office' => 'phone_work',
                            ),
                            'required' => false,
                        ),
                    ),
                    1 =>
                    array (
                        'name' => 'c_contacts_contacts_1_name',
                        'label' => 'LBL_CONTACT_PARENT_NAME',

                        'displayParams' =>
                        array (
                            'field_to_name_array' =>
                            array (
                                'id' => 'account_id',
                                'name' => 'c_contacts_contacts_1_name',
                                'id' => 'c_contacts_contacts_1c_contacts_ida',
                                'position' => 'contact_position',
                                'mobile_phone' => 'contact_mobile',
                            ),
                        ),
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'branch',
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'position',
                        'label' => 'LBL_POSITION',
                    ),
                    1 => 'contact_position',
                ),
                8 =>
                array (
                    0 =>
                    array (
                        'name' => 'experience_year',
                        'label' => 'LBL_EXPERIENCE_YEAR',
                    ),
                    1 => 'contact_mobile',
                ),
            ),
            'lbl_panel_assignment' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'lead_source',
                        'customCode' => '{$lead_source}',
                    ),
                    1 =>
                    array (
                        'name' => 'contact_status',
                        'customCode' => '{$STATUS}',
                    ),
                ),
                1 =>
                array (
                    0 => 'lead_source_description',
                    1 => 'campaign_name',
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'displayParams' =>
                        array (
                            'required' => true,
                        ),
                    ),
                    1 => 'team_name',
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_writing',
                        'label' => 'LBL_TS_WRITING',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_interviewing',
                        'label' => 'LBL_TS_INTERVIEWING',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_average',
                        'label' => 'LBL_TS_AVERAGE',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_position',
                        'studio' => 'visible',
                        'label' => 'LBL_TS_POSITION',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_wish_locate',
                        'label' => 'LBL_TS_LOCATE',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_note',
                        'label' => 'LBL_TS_WISH_JOB',
                    ),
                ),
            ),
            'lbl_editview_panel2' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_bank',
                        'label' => 'LBL_TT_BANK',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_bank_branch',
                        'label' => 'LBL_TT_BANK_BRANCH',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_bank_location',
                        'label' => 'LBL_TT_BANK_LOCATION',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_person',
                        'label' => 'LBL_TT_PERSON',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_phone',
                        'label' => 'LBL_TT_PHONE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_email',
                        'label' => 'LBL_TT_EMAIL',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_note',
                        'label' => 'LBL_TT_NOTE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_corebanking',
                        'label' => 'LBL_TT_COREBANKING',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_skill',
                        'label' => 'LBL_TT_SKILL',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_strong',
                        'label' => 'LBL_TT_STRONG',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_how_increse',
                        'label' => 'LBL_TT_HOW_INREASE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_award',
                        'label' => 'LBL_TT_AWARD',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_creation',
                        'label' => 'LBL_TT_CREATION',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_suggest',
                        'label' => 'LBL_TT_SUGGEST',
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_intership_score',
                        'label' => 'LBL_INTERNSHIP_SCORE',
                    ),
                ),
            ),
            'lbl_editview_panel3' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_graduated_score',
                        'label' => 'LBL_GRADUATED_SCORE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_order',
                        'label' => 'LBL_TT_ORDER',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'vl_bank',
                        'label' => 'LBL_VL_BANK',
                    ),
                    1 =>
                    array (
                        'name' => 'vl_bank_unit',
                        'label' => 'LBL_VL_BANK_UNIT',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'vl_position',
                        'label' => 'LBL_VL_POSITION',
                    ),
                    1 =>
                    array (
                        'name' => 'vl_date',
                        'label' => 'LBL_VL_DATE',
                    ),
                ),
            ),
        ),
    ),
);
