<?php
$viewdefs['Contacts'] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'hidden' =>
                array (
                    0 => '<input type="hidden" name="password" id="password" value="">',
                ),
                'hideAudit' => true,
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DELETE',
                    2 =>
                    array (
                        'customCode' => '{$CUSTOM_BUTTON}',
                    ),
                    //          5 =>
                    //          array (
                    //            'customCode' => '{$send_survey}',
                    //          ),
                    //          6 =>
                    //          array (
                    //            'customCode' => '{$send_poll}',
                    //          ),
                ),
            ),
            'maxColumns' => '2',
            'useTabs' => true,
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '
            {sugar_getscript file="custom/modules/Contacts/js/detailviews.js"}
            {sugar_getscript file="custom/modules/Contacts/js/pGenerator.jquery.js"}
            {sugar_getscript file="custom/modules/Contacts/js/handlePTDemoContact.js"}',
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_COMPANY' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL1' =>
                array (
                    'newTab' => true,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL2' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_EDITVIEW_PANEL3' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'contact_id',
                        'label' => 'LBL_CONTACT_ID',
                        'customCode' => '<span class="textbg_blue">{$fields.contact_id.value}</span>',
                    ),
                    1 => 'type'
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_student_name',
                        'label' => 'LBL_FULL_NAME',
                    ),
                    1 => 'picture',
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'gender',
                        'studio' => 'visible',
                        'label' => 'LBL_GENDER',
                    ),
                    1 =>
                    array (
                        'name' => 'birthdate',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'email1',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL_ADDRESS',
                    ),
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'label' => 'LBL_PRIMARY_ADDRESS',
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                        ),
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'phone_mobile',
                        'label' => 'LBL_MOBILE_PHONE',
                    ),
                    1 =>
                    array (
                        'name' => 'other_mobile',
                    ),
                ),
                5 =>
                array (
                    0 => 'facebook',
                    1 =>
                    array (
                        'name' => 'phone_other',
                        'comment' => 'Other phone number for the contact',
                        'label' => 'LBL_OTHER_PHONE',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'description',
                        'comment' => 'Full text of the note',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                    1 => 'do_not_call'
                ),
            ),
            'lbl_panel_company' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_number',
                        'label' => 'LBL_INDENTITY_NUMBER',
                    ),
                    1 =>
                    array (
                        'name' => 'alt_address_street',
                        'label' => 'LBL_ALTERNATE_ADDRESS',
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'alt',
                        ),
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_date',
                        'label' => 'LBL_INDENTITY_DATE',
                    ),
                    1 =>
                    array (
                        'name' => 'weight',
                        'label' => 'LBL_WEIGHT',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_location',
                        'label' => 'LBL_INDENTITY_LOCATION',
                    ),
                    1 =>
                    array (
                        'name' => 'height',
                        'label' => 'LBL_HEIGHT',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_school_contacts_1_name',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_year',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_YEAR',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'graduated_major',
                        'label' => 'LBL_GRADUATED_MAJOR',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_rate',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_RATE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'account_name',
                    ),
                    1 =>
                    array (
                        'name' => 'c_contacts_contacts_1_name',
                        'label' => 'LBL_CONTACT_PARENT_NAME',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'branch',
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'position',
                        'label' => 'LBL_POSITION',
                    ),
                    1 => 'contact_position',
                ),

                8 =>
                array (
                    0 =>
                    array (
                        'name' => 'experience_year',
                        'label' => 'LBL_EXPERIENCE_YEAR',
                    ),
                    1 => 'contact_mobile',
                ),
            ),
            'lbl_panel_assignment' =>
            array (
                0 =>
                array (
                    0 =>array (
                        'name' => 'lead_source',
                        'customCode' => '{$fields.lead_source.value}{if !empty($fields.activity.value)} - {$fields.activity.value}{/if}',
                    ),
                    1 =>
                    array (
                        'name' => 'contact_status',
                        'customCode' => '{$COLOR}',
                    ),
                ),
                1 =>
                array (
                    0 => 'lead_source_description',
                    1 =>
                    array (
                        'name' => 'campaign_name',
                        'label' => 'LBL_CAMPAIGN',
                    ),
                ),
                2 => array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'customCode' => '{$assigned_user_idQ}'
                    ),
                    1 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                3 => array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
            'lbl_editview_panel1' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_writing',
                        'label' => 'LBL_TS_WRITING',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_interviewing',
                        'label' => 'LBL_TS_INTERVIEWING',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_average',
                        'label' => 'LBL_TS_AVERAGE',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_position',
                        'studio' => 'visible',
                        'label' => 'LBL_TS_POSITION',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'ts_wish_locate',
                        'label' => 'LBL_TS_LOCATE',
                    ),
                    1 =>
                    array (
                        'name' => 'ts_note',
                        'label' => 'LBL_TS_WISH_JOB',
                    ),
                ),
            ),
            'lbl_editview_panel2' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_bank',
                        'label' => 'LBL_TT_BANK',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_bank_branch',
                        'label' => 'LBL_TT_BANK_BRANCH',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_bank_location',
                        'label' => 'LBL_TT_BANK_LOCATION',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_person',
                        'label' => 'LBL_TT_PERSON',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_phone',
                        'label' => 'LBL_TT_PHONE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_email',
                        'label' => 'LBL_TT_EMAIL',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_note',
                        'label' => 'LBL_TT_NOTE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_corebanking',
                        'label' => 'LBL_TT_COREBANKING',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_skill',
                        'label' => 'LBL_TT_SKILL',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_strong',
                        'label' => 'LBL_TT_STRONG',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_how_increse',
                        'label' => 'LBL_TT_HOW_INREASE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_award',
                        'label' => 'LBL_TT_AWARD',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_creation',
                        'label' => 'LBL_TT_CREATION',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_suggest',
                        'label' => 'LBL_TT_SUGGEST',
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_intership_score',
                        'label' => 'LBL_INTERNSHIP_SCORE',
                    ),
                ),
            ),
            'lbl_editview_panel3' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'tt_graduated_score',
                        'label' => 'LBL_GRADUATED_SCORE',
                    ),
                    1 =>
                    array (
                        'name' => 'tt_order',
                        'label' => 'LBL_TT_ORDER',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'vl_bank',
                        'label' => 'LBL_VL_BANK',
                    ),
                    1 =>
                    array (
                        'name' => 'vl_bank_unit',
                        'label' => 'LBL_VL_BANK_UNIT',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'vl_position',
                        'label' => 'LBL_VL_POSITION',
                    ),
                    1 =>
                    array (
                        'name' => 'vl_date',
                        'label' => 'LBL_VL_DATE',
                    ),
                ),
            ),
        ),
    ),
);
