<?php
$viewdefs['Leads'] =
array (
    'DetailView' =>
    array (
        'templateMeta' =>
        array (
            'form' =>
            array (
                'buttons' =>
                array (
                    0 => 'EDIT',
                    1 => 'DUPLICATE',
                    2 => 'DELETE',
                    3 =>
                    array (
                        'customCode' => '{$btn_convert_2}',
                    ),
//                    4 =>
//                    array (
//                        'customCode' => '{$send_survey}',
//                    ),
//                    5 =>
//                    array (
//                        'customCode' => '{$send_poll}',
//                    ),
                ),
                'headerTpl' => 'modules/Leads/tpls/DetailViewHeader.tpl',
            ),
            'maxColumns' => '2',
            'useTabs' => true,
            'widths' =>
            array (
                0 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
                1 =>
                array (
                    'label' => '10',
                    'field' => '30',
                ),
            ),
            'javascript' => '
            {sugar_getscript file="custom/modules/Leads/js/addToPT.js"}
            {sugar_getscript file="modules/Leads/Lead.js"}
            ',
            'tabDefs' =>
            array (
                'LBL_CONTACT_INFORMATION' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_COMPANY' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
                'LBL_PANEL_ASSIGNMENT' =>
                array (
                    'newTab' => false,
                    'panelDefault' => 'expanded',
                ),
            ),
            'syncDetailEditViews' => true,
        ),
        'panels' =>
        array (
            'lbl_contact_information' =>
            array (
                0 =>
                array (
                    0 =>
                    array (
                        'name' => 'full_lead_name',
                        'label' => 'LBL_FULL_NAME',
                    ),
                    1 => 'type'
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'gender',
                        'studio' => 'visible',
                        'label' => 'LBL_GENDER',
                    ),
                    1 =>
                    array (
                        'name' => 'birthdate',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'email1',
                        'studio' => 'false',
                        'label' => 'LBL_EMAIL_ADDRESS',
                    ),
                    1 =>
                    array (
                        'name' => 'primary_address_street',
                        'label' => 'LBL_PRIMARY_ADDRESS',
                        'type' => 'address',
                        'displayParams' =>
                        array (
                            'key' => 'primary',
                        ),
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'phone_mobile',
                        'label' => 'LBL_MOBILE_PHONE',
                    ),
                    1 =>
                    array (
                        'name' => 'other_mobile',
                    ),
                ),
                4 =>
                array (
                    0 => 'facebook',
                    1 =>
                    array (
                        'name' => 'phone_other',
                        'comment' => 'Other phone number for the contact',
                        'label' => 'LBL_OTHER_PHONE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array (
                        'name' => 'description',
                        'comment' => 'Full text of the note',
                        'label' => 'LBL_DESCRIPTION',
                    ),
                    1 => 'do_not_call'
                ),
            ),
            'lbl_panel_company' =>
            array (
                0 =>
                array (
                    0 => 'identity_number',
                    1 =>
                    array (
                        'name' => 'alt_address_street',
                        'label' => 'LBL_ALTERNATE_ADDRESS',
                    ),
                ),
                1 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_date',
                        'label' => 'LBL_INDENTITY_DATE',
                    ),
                    1 =>
                    array (
                        'name' => 'weight',
                        'label' => 'LBL_WEIGHT',
                    ),
                ),
                2 =>
                array (
                    0 =>
                    array (
                        'name' => 'identity_location',
                        'label' => 'LBL_INDENTITY_LOCATION',
                    ),
                    1 =>
                    array (
                        'name' => 'height',
                        'label' => 'LBL_HEIGHT',
                    ),
                ),
                3 =>
                array (
                    0 =>
                    array (
                        'name' => 'j_school_leads_1_name',
                        'label' => 'LBL_SCHOOL_NAME',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_year',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_YEAR',
                    ),
                ),
                4 =>
                array (
                    0 =>
                    array (
                        'name' => 'graduated_major',
                        'label' => 'LBL_GRADUATED_MAJOR',
                    ),
                    1 =>
                    array (
                        'name' => 'graduated_rate',
                        'studio' => 'visible',
                        'label' => 'LBL_GRADUATED_RATE',
                    ),
                ),
                5 =>
                array (
                    0 =>
                    array(
                        'name' => 'account_name',
                    ),
                    1 =>
                    array (
                        'name' => 'c_contacts_leads_1_name',
                        'label' => 'LBL_GUARDIAN_NAME',
                    ),
                ),
                6 =>
                array (
                    0 =>
                    array (
                        'name' => 'branch',
                    ),
                ),
                7 =>
                array (
                    0 =>
                    array (
                        'name' => 'position',
                        'label' => 'LBL_POSITION',
                    ),
                    1 =>
                    array (
                        'name' => 'experience_year',
                        'label' => 'LBL_EXPERIENCE_YEAR',
                    ),
                ),
            ),
            'lbl_panel_assignment' =>
            array (
                0 =>
                array (
                    0 =>array (
                        'name' => 'lead_source',
                        'customCode' => '{$fields.lead_source.value}{if !empty($fields.activity.value)} - {$fields.activity.value}{/if}',
                    ),
                    1 =>
                    array (
                        'name' => 'status',
                        'customCode' => '{if $fields.status.value == "New"}
                        <span class="textbg_green"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "Assigned"}
                        <span class="textbg_bluelight"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "In Process"}
                        <span class="textbg_blue"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "Converted"}
                        <span class="textbg_red"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "PT/Demo"}
                        <span class="textbg_violet"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "Recycled"}
                        <span class="textbg_orange"><b>{$fields.status.value}<b></span>

                        {elseif $fields.status.value == "Dead"}
                        <span class="textbg_black"><b>{$fields.status.value}<b></span>

                        {else}
                        <span><b>{$fields.status.value}<b></span>
                        {/if}',
                    ),
                ),
                1 =>
                array (
                    0 => 'lead_source_description',
                    1 =>
                    array (
                        'name' => 'campaign_name',
                        'label' => 'LBL_CAMPAIGN',
                    ),
                ),
                3 =>
                array (
                    0 => '',
                    1 =>
                    array (
                        'name' => 'potential',
                        'studio' => 'visible',
                        'label' => 'LBL_POTENTIAL',
                        'customCode' => '{$potentialQ}',
                    ),
                ),
                                2 => array (
                    0 =>
                    array (
                        'name' => 'assigned_user_name',
                        'customCode' => '{$assigned_user_idQ}'
                    ),
                    1 => array (
                        'name' => 'date_entered',
                        'customCode' => '{$fields.date_entered.value} {$APP.LBL_BY} {$fields.created_by_name.value}',
                        'label' => 'LBL_DATE_ENTERED',
                    ),
                ),
                4 => array (
                    0 => 'team_name',
                    1 =>
                    array (
                        'name' => 'date_modified',
                        'customCode' => '{$fields.date_modified.value} {$APP.LBL_BY} {$fields.modified_by_name.value}',
                        'label' => 'LBL_DATE_MODIFIED',
                    ),
                ),
            ),
        ),
    ),
);
