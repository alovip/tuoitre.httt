<?php
// created: 2016-02-19 02:09:18
$mod_strings = array (
    'LBL_GENDER' => 'Gender',
    'LBL_NATIONALITY' => 'Nationality',
    'LBL_OCCUPATION' => 'Occupation',
    'LBL_POTENTIAL' => 'Potential level',
    'LBL_SCORE' => 'Testing Score',
    'LBL_REPORTS_TO' => 'Reports To:',
    'LBL_CONTACTS' => 'Students',
    'LBL_LEADS' => 'Leads',
    'LBL_CONTACT_ID' => 'Student ID',
    'LBL_CONVERTED_CONTACT' => 'Converted Student:',
    'LBL_OPPORTUNITIES' => 'Enrollments',
    'LBL_OPPORTUNITY_AMOUNT' => 'Enrollment Amount:',
    'LBL_OPPORTUNITY_ID' => 'Enrollment ID',
    'LBL_OPPORTUNITY_NAME' => 'Enrollment Name:',
    'LBL_CONVERTED_OPP' => 'Converted Enrollment:',
    'LBL_SLL_DATE' => 'Testing Date',
    'LBL_SLL_TITLE' => 'Testing List',
    'LBL_SLL_SEARCH' => 'Find Students',
    'LBL_PANEL_GUARDIAN' => 'Contact Info',
    'LBL_GUARDIAN_NAME' => 'Parent',
    'LBL_GUARDIAN_EMAIL' => 'Contact Email',
    'LBL_GUARDIAN_PHONE' => 'Parent Phone',
    'LBL_CREATE_OPPORTUNITIES' => 'Create Enrollment',
    'LBL_MOBILE_PHONE' => 'Mobile:',
    'LBL_EMAIL_ADDRESS' => 'Email Address:',
    'LBL_ACCOUNT_NAME' => 'Company Name',
    'LBL_ACCOUNT' => 'Corporate',
    'LBL_ACCOUNT_DESCRIPTION' => 'Corporate Description',
    'LBL_ACCOUNT_ID' => 'Corporate ID',
    'LBL_LIST_ACCOUNT_NAME' => 'Corporate Name',
    'LBL_CONVERTED_ACCOUNT' => 'Converted Corporate:',
    'LNK_SELECT_ACCOUNTS' => ' OR Select Corporate',
    'LNK_NEW_ACCOUNT' => 'Create Corporate',
    'LBL_DELIVERY_REVENUE' => 'Delivery',
    'LBL_CARRYFORWARD' => 'CarryForward',
    'LBL_SCHOOL_NAME' => 'School',
    'LBL_NICK_NAME' => 'Nick-name',
    'LBL_OTHER_MOBILE' => 'Other Mobile 1',
    'LBL_OTHER_PHONE' => 'Other Mobile 2',
    'LBL_PANEL_ADVANCED' => 'Others',
    'LBL_PRIMARY_STREET' => 'Street',
    'LBL_ALT_STREET' => 'Street',
    'LBL_PREFERRED_KIND_OF_COURSE' => 'Prefered Level',
    'LBL_BIRTHDATE' => 'Birthdate:',
    'LBL_STATUS' => 'Status:',
    'LNK_OTHERS' => 'Others',
    'LNK_CONTACT_INFO' => 'Contact Info',
    'LBL_OFFICE_PHONE' => 'Company Phone:',
    'LBL_CONVERTLEAD_TITLE' => 'Convert To Student',
    'LBL_CONVERTLEAD' => 'Convert To Student',
    'LBL_SCHOOL_NOTI' => 'School',
    'LBL_CONTACT_NAME_NOTI' => 'Contact / Parent Name',
    'LBL_DETAILVIEW_PANEL1' => 'New Panel 1',
    'LBL_MEETINGS' => 'Meetings',
    'LBL_EC_NOTE' => 'Ec\'s Note',
    'LBL_TEACHER_COMMENT' => 'Teacher\'s Comment',
    'LBL_LEADS_J_PTRESULT_1_FROM_J_PTRESULT_TITLE' => 'Placement Test',
    'LBL_FIRST_NAME' => 'First Name:',
    'LBL_LEAD_SOURCE' => 'Source',
    'LBL_REMOVE' => 'Remove',
    'LBL_J_PTRESULT_LEADS_1_FROM_J_PTRESULT_TITLE' => 'Placement Test',
    'LBL_LEAD_DEMO' => 'Demo',
    'LBL_LEAD_PT' => 'Placement Test',
    'LBL_TEAM_TYPE' => 'Team Type',
    'LBL_RELATIONSHIP' => 'Other Family Relationship',
    'LBL_DESCRIBE_RELATIONSHIP' => 'Describle Other Family </br> Relationship',
    'LBL_CONTACT_RELA' => 'Main Relationship',
    'LNK_CONTACT' => ' ',
    'LBL_STUDENT_SITUATION' => 'Student Situation',
    'LBL_J_CLASS_LEADS_1_FROM_J_CLASS_TITLE' => 'Class',
    'LBL_DESCRIPTION' => 'Description:',
    'LBL_ERROR' => 'Current student is exist in this shedule!',
    'LBL_COMPLETE' => 'Thêm học viên thành công!',
    'LBL_LEAD_SMS' => 'Lead SMS',
    'LBL_ASSISTANT' => 'Name No Marks',

    'LBL_CONTACT_INFORMATION' => 'Overview',
    'LBL_PANEL_COMPANY' => 'Contact Info',
    'LBL_PANEL_ASSIGNMENT' => 'Other',
    'LBL_LEAD_SITUATIONS' => 'Lead Situations',
);