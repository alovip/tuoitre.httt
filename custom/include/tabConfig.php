<?php
// created: 2017-11-18 13:03:13
$GLOBALS['tabStructure'] = array (
  'LBL_TABGROUP_SALES' => 
  array (
    'label' => 'LBL_TABGROUP_SALES',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Prospects',
      2 => 'Leads',
      3 => 'Contacts',
      4 => 'J_Payment',
      5 => 'J_Class',
      6 => 'Meetings',
      7 => 'Documents',
      8 => 'J_StudentSituations',
      9 => 'Tasks',
      10 => 'Calls',
      11 => 'J_Feedback',
      12 => 'Notifications',
      13 => 'J_PaymentDetail',
      14 => 'J_Sponsor',
      15 => 'Campaigns',
      16 => 'J_Marketingplan',
      17 => 'ProspectLists',
      18 => 'C_Contacts',
      19 => 'J_School',
      20 => 'C_Teachers',
      21 => 'Accounts',
      22 => 'Contracts',
      23 => 'J_Teachercontract',
      24 => 'J_Gradebook',
      25 => 'J_GradebookConfig',
      26 => 'ProductTemplates',
      27 => 'Reports',
    ),
  ),
  'LBL_GROUPTAB2_1480343614' => 
  array (
    'label' => 'LBL_GROUPTAB2_1480343614',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Leads',
      2 => 'Contacts',
      3 => 'Accounts',
      4 => 'Contracts',
      5 => 'Documents',
      6 => 'C_Contacts',
      7 => 'J_Class',
      8 => 'J_Payment',
      9 => 'Meetings',
      10 => 'Reports',
    ),
  ),
  'LBL_GROUPTAB3_1486717995' => 
  array (
    'label' => 'LBL_GROUPTAB3_1486717995',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Emails',
      2 => 'ProspectLists',
      3 => 'J_Voucher',
      4 => 'Campaigns',
      5 => 'J_Marketingplan',
      6 => 'J_Sponsor',
      7 => 'KBDocuments',
      8 => 'Reports',
    ),
  ),
  'LBL_GROUPTAB3_1508387079' => 
  array (
    'label' => 'LBL_GROUPTAB3_1508387079',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'J_Class',
      2 => 'Meetings',
      3 => 'C_Teachers',
      4 => 'J_Teachercontract',
      5 => 'Calendar',
      6 => 'Reports',
    ),
  ),
  'LBL_GROUPTAB3_1508291017' => 
  array (
    'label' => 'LBL_GROUPTAB3_1508291017',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'KBDocuments',
      2 => 'Documents',
    ),
  ),
  'LBL_GROUPTAB4_1487735757' => 
  array (
    'label' => 'LBL_GROUPTAB4_1487735757',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'Reports',
    ),
  ),
  'LBL_GROUPTAB4_1442479371' => 
  array (
    'label' => 'LBL_GROUPTAB4_1442479371',
    'modules' => 
    array (
      0 => 'Home',
      1 => 'J_Kindofcourse',
      2 => 'J_ConfigInvoiceNo',
      3 => 'J_Coursefee',
      4 => 'J_Discount',
      5 => 'J_Partnership',
      6 => 'J_Targetconfig',
      7 => 'C_HelpTextConfig',
      8 => 'J_GradebookConfig',
      9 => 'C_KeyboardSetting',
      10 => 'Holidays',
      11 => 'C_DuplicationDetection',
    ),
  ),
);