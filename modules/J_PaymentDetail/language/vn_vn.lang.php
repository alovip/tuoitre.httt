<?php
/*********************************************************************************
* By installing or using this file, you are confirming on behalf of the entity
* subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
* the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
* http://www.sugarcrm.com/master-subscription-agreement
*
* If Company is not bound by the MSA, then by installing or using this file
* you are agreeing unconditionally that Company will be bound by the MSA and
* certifying that you have authority to bind Company accordingly.
*
* Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
********************************************************************************/

$mod_strings = array (
    'LBL_TEAM' => 'Chi nhánh',
    'LBL_TEAMS' => 'Chi nhánh',
    'LBL_TEAM_ID' => 'Chi nhánh Id',
    'LBL_ASSIGNED_TO_ID' => 'Người phụ trách',
    'LBL_ASSIGNED_TO_NAME' => 'Người phụ trách',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Ngày tạo',
    'LBL_DATE_MODIFIED' => 'Ngày sửa',
    'LBL_MODIFIED' => 'Sửa bởi',
    'LBL_MODIFIED_ID' => 'Sửa bởi Id',
    'LBL_MODIFIED_NAME' => 'Sửa bởi',
    'LBL_CREATED' => 'Tạo bởi',
    'LBL_CREATED_ID' => 'Tạo bởi Id',
    'LBL_DESCRIPTION' => 'Ghi chú thêm',
    'LBL_DELETED' => 'Đã xóa',
    'LBL_NAME' => 'No.',
    'LBL_CREATED_USER' => 'Tạo bởi User',
    'LBL_MODIFIED_USER' => 'Sửa bởi User',
    'LBL_LIST_NAME' => 'ID',
    'LBL_EDIT_BUTTON' => 'Sửa',
    'LBL_REMOVE' => 'Xóa',
    'LBL_METHOD_NOTE' => 'Ghi chú thêm',
    'LBL_LIST_FORM_TITLE' => 'Danh sách Chi tiết thanh toán',
    'LBL_MODULE_NAME' => 'Chi tiết thanh toán',
    'LBL_MODULE_TITLE' => 'Chi tiết thanh toán',
    'LBL_HOMEPAGE_TITLE' => 'Chi tiết thanh toán',
    'LNK_NEW_RECORD' => 'Tạo mới',
    'LNK_LIST' => 'View Payment Detail',
    'LNK_IMPORT_J_PAYMENTDETAIL' => 'Import Payment Detail',
    'LBL_SEARCH_FORM_TITLE' => 'Search Payment Detail',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'Xem lịch sử',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Hoạt động',
    'LBL_J_PAYMENTDETAIL_SUBPANEL_TITLE' => 'Payment Detail',
    'LBL_NEW_FORM_TITLE' => 'Tạo mới thanh toán',
    'LBL_PAYMENT_NAME' => 'Thanh toán',
    'LBL_PAYMENT_ID' => 'Thanh toán ID',
    'LBL_METHOD_FEE' => 'Phí thanh toán',
    'LBL_PAYMENT_DATE' => 'Ngày',
    'LBL_PAYMENT_METHOD' => 'Hình thức TT',
    'LBL_CARD_TYPE' => 'Loại thẻ',
    'LBL_INVOICE_NUMBER' => 'Số hóa đơn',
    'LBL_INVOICE_NUMBER_INT' => 'Số hóa đơn (Số)',
    'LBL_SERIAL_NO' => 'Ký hiệu hóa đơn',
    'LBL_BEFORE_DISCOUNT' => 'Tổng tiền trước Discount',
    'LBL_DISCOUNT_AMOUNT' => 'Discount',
    'LBL_SPONSOR_AMOUNT' => 'Sponsor',
    'LBL_PAYMENT_NO'         => 'Số',
    'LBL_CONTENT_VAT_INVOICE'         => 'Nội dung trên HD',
    'LBL_SALE_TYPE'                 => 'Loại sale',
    'LBL_SALE_TYPE_DATE'            => 'Ngày tính commission',
    'LBL_IS_FIRST'                  => 'Lần thu đầu tiền',
    'LBL_CONTRACT_NAME'                  => 'Hợp đồng',
    'LBL_CONTRACT_ID'                  => 'Hợp đồng ID',
    'LBL_EXPRECTED_PAYMENT_DATE'      => 'Ngày thu dự kiến',
    'LBL_INVOICE_DATE'      => 'Ngày xuất HĐ',
    'LBL_AGING_REPORT'      => 'Tuổi nợ',
    'LBL_PERCENT_COMPLETE'      => '% hoàn thành',
);