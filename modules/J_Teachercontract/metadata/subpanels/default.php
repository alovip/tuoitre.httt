<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * By installing or using this file, you are confirming on behalf of the entity
 * subscribed to the SugarCRM Inc. product ("Company") that Company is bound by
 * the SugarCRM Inc. Master Subscription Agreement (“MSA”), which is viewable at:
 * http://www.sugarcrm.com/master-subscription-agreement
 *
 * If Company is not bound by the MSA, then by installing or using this file
 * you are agreeing unconditionally that Company will be bound by the MSA and
 * certifying that you have authority to bind Company accordingly.
 *
 * Copyright (C) 2004-2013 SugarCRM Inc.  All rights reserved.
 ********************************************************************************/


$module_name='J_Teachercontract';
$subpanel_layout = array(
	'top_buttons' => array(
		array('widget_class' => 'SubPanelTopCreateButton'),
		array('widget_class' => 'SubPanelTopSelectButton', 'popup_module' => $module_name),
	),

	'where' => '',

	'list_fields' => array(
'name' =>
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '15%',
    'default' => true,
  ),
  'c_teachers_j_teachercontract_1_name' =>
  array (
    'vname' => 'LBL_C_TEACHERS_J_TEACHERCONTRACT_1_FROM_C_TEACHERS_TITLE',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '10%',
    'default' => true,
  ),
  'contract_type' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_CONTRACT_TYPE',
    'width' => '7%',
  ),
  'contract_date' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_CONTRACT_DATE',
    'width' => '10%',
    'default' => true,
  ),
  'contract_until' =>
  array (
    'type' => 'date',
    'vname' => 'LBL_CONTRACT_UNTIL',
    'width' => '10%',
    'default' => true,
  ),
  'status' =>
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_STATUS',
    'width' => '10%',
  ),
  'time_up' =>
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_TIME_UP',
    'width' => '5%',
  ),  
  'price' =>
  array (
    'type' => 'decimal',
    'default' => true,
    'vname' => 'LBL_PRICE',
    'width' => '10%',
  ),
  'date_modified' =>
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' =>
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'J_Teachercontract',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' =>
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'J_Teachercontract',
    'width' => '5%',
    'default' => true,
  ),
	),
);

?>